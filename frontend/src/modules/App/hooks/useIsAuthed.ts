import useAppContext from "@src/modules/App/hooks/useAppContext";

const useIsAuthed = () => {
  const { userToken } = useAppContext();
  return !!userToken;
};

export default useIsAuthed;
