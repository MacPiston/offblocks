import React, { FunctionComponent, useState } from "react";
import {
  AppBar,
  IconButton,
  Button,
  Container,
  Toolbar,
  Box,
  Avatar,
  Tooltip,
} from "@mui/material";

import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import useNewDraftId from "@src/modules/Shared/hooks/useNewDraftId";
import useNotifications from "@src/modules/Shared/hooks/useNotifications";
import useAppContext from "@src/modules/App/hooks/useAppContext";

import { DRAWER_WIDTH } from "@src/modules/App/AppDrawer/AppDrawer.components";
import { StyledLogo } from "@src/modules/Shared/components/Logo";
import ProfileMenu from "@src/modules/App/AppTopbar/ProfileMenu";

import MenuIcon from "@mui/icons-material/Menu";

interface TopbarChip {
  label: string;
  tooltipMessage: string;
  onClick: () => void;
}

interface AppTopbarProps {
  toggleMobileDrawer: () => void;
}

export const TOPBAR_HEIGHT_MOBILE = 56;
export const TOPBAR_HEIGHT = 64;

const AppTopbar: FunctionComponent<AppTopbarProps> = ({
  toggleMobileDrawer,
}) => {
  const context = useAppContext();
  const { error } = useNotifications();
  const { toFlights, toAircrafts, toRegister, toDraft } = useRedirects();
  const newDraftMutation = useNewDraftId();

  const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);

  const authedLinks: TopbarChip[] = [
    {
      label: "Flights",
      tooltipMessage: "View and manage flights",
      onClick: toFlights,
    },
    {
      label: "Aircrafts",
      tooltipMessage: "View and manage aircrafts",
      onClick: toAircrafts,
    },
  ];
  const publicLinks: TopbarChip[] = [];
  const links = !!context.userToken ? authedLinks : publicLinks;

  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) =>
    setAnchorElUser(event.currentTarget);

  const handleCloseUserMenu = () => setAnchorElUser(null);

  const handleNewFlight = () => {
    if (!!context.userToken) {
      newDraftMutation.mutate(undefined, {
        onSuccess: (response) => {
          toDraft(response.data._id);
        },
        onError: () => {
          error("Error occurred");
        },
      });
    } else toRegister();
  };

  return (
    <AppBar
      position="fixed"
      sx={{
        width: { lg: `calc(100% - ${DRAWER_WIDTH}px)` },
        ml: { lg: `${DRAWER_WIDTH}px` },
        boxShadow: 0,
      }}
    >
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Box sx={{ flexGrow: 1, display: { xs: "flex", lg: "none" } }}>
            <IconButton onClick={toggleMobileDrawer}>
              <MenuIcon />
            </IconButton>
          </Box>

          <Box sx={{ flexGrow: 1, display: { xs: "flex", lg: "none" } }}>
            <StyledLogo />
          </Box>

          <Box sx={{ flexGrow: 1, display: { xs: "none", lg: "flex" } }}>
            {links.map((link, index) => (
              <Tooltip key={index} title={link.tooltipMessage}>
                <Button sx={{ color: "white", mr: 1 }} onClick={link.onClick}>
                  {link.label}
                </Button>
              </Tooltip>
            ))}
            <Tooltip title="Create and edit new flight">
              <Button
                variant="contained"
                color="success"
                onClick={handleNewFlight}
              >
                New flight
              </Button>
            </Tooltip>
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <Avatar
                  alt={context.userToken?.email}
                  src="/static/images/avatar/2.jpg"
                />
              </IconButton>
            </Tooltip>
            <ProfileMenu
              sx={{ mt: "45px" }}
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={!!anchorElUser}
              onClose={handleCloseUserMenu}
            />
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default AppTopbar;
