import React, { useMemo } from "react";
import { CssBaseline } from "@mui/material";

import useLogoutRequestHandler from "@src/common/eventHandlers/useLogoutRequestHandler";
import useTokenChangeHandler from "@src/common/eventHandlers/useTokenChangeHandler";

import AppContext, { AppContextType } from "@src/modules/App/AppContext";
import AppRouter from "@src/modules/Routing/AppRouter";

const App = () => {
  const [token] = useTokenChangeHandler();
  useLogoutRequestHandler();

  const context: AppContextType = useMemo(
    () => ({
      userToken: token,
    }),
    [token],
  );

  return (
    <AppContext.Provider value={context}>
      <CssBaseline />
      <AppRouter />
    </AppContext.Provider>
  );
};

export default App;
