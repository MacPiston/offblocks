import React, { FunctionComponent, ReactNode } from "react";
import { useMatch, useResolvedPath } from "react-router-dom";

import { ListItemButton, ListItemIcon, ListItemText } from "@mui/material";

export interface DrawerLinkProps {
  text?: string;
  icon?: ReactNode;
  action?:
    | { link: string; onClick?: never }
    | { link?: never; onClick: () => void };
  matchedRoute?: string;
  addAction?: boolean;
}

const DrawerLink: FunctionComponent<DrawerLinkProps> = ({
  text,
  icon,
  action,
  matchedRoute,
  addAction,
}) => {
  const resolved = useResolvedPath(matchedRoute ?? "");
  const match = useMatch({ path: resolved.pathname, end: true });

  return (
    <ListItemButton
      sx={{
        "&:hover": {
          backgroundColor: addAction ? "success.main" : "primary.light",
        },
      }}
      selected={!!match}
      key={text}
      onClick={action?.onClick}
    >
      {icon && <ListItemIcon sx={{ color: "white" }}>{icon}</ListItemIcon>}
      <ListItemText
        sx={{ color: "white", textShadow: "1px 1px 3px rgba(0,0,0,0.36)" }}
        primary={text}
      />
    </ListItemButton>
  );
};

export default DrawerLink;
