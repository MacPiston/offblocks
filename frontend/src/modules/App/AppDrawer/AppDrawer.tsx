import React, { FunctionComponent } from "react";
import { Box } from "@mui/material";

import AppDrawerContent from "@src/modules/App/AppDrawer/AppDrawerContent";
import {
  DRAWER_WIDTH,
  StyledDrawer,
} from "@src/modules/App/AppDrawer/AppDrawer.components";
import { useIsMobile } from "@src/modules/Shared/hooks/useIsMobile";

interface AppDrawerProps {
  mobileDrawerOpen: boolean;
  toggleMobileDrawer: () => void;
  closeMobileDrawer: () => void;
}

const AppDrawer: FunctionComponent<AppDrawerProps> = ({
  mobileDrawerOpen,
  toggleMobileDrawer,
  closeMobileDrawer,
}) => {
  const isMobile = useIsMobile();

  return (
    <Box sx={{ width: { lg: DRAWER_WIDTH } }}>
      <StyledDrawer
        variant={isMobile ? "temporary" : "permanent"}
        open={mobileDrawerOpen || !isMobile}
        onClose={toggleMobileDrawer}
        ModalProps={{
          keepMounted: isMobile,
        }}
        sx={{
          display: isMobile
            ? { xs: "block", lg: "none" }
            : { xs: "none", lg: "block" },
        }}
      >
        <AppDrawerContent closeMobileDrawer={closeMobileDrawer} />
      </StyledDrawer>
    </Box>
  );
};

export default AppDrawer;
