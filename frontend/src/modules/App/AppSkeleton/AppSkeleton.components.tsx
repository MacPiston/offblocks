import { Box, styled } from "@mui/material";

import {
  TOPBAR_HEIGHT,
  TOPBAR_HEIGHT_MOBILE,
} from "@src/modules/App/AppTopbar/AppTopbar";

export const Wrapper = styled(Box)`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  padding-top: ${TOPBAR_HEIGHT_MOBILE}px;

  ${({ theme }) => theme.breakpoints.up("sm")} {
    padding-top: ${TOPBAR_HEIGHT}px;
  }
`;

export const PageContentWrapper = styled(Box)`
  position: relative;
  height: calc(100vh - ${TOPBAR_HEIGHT_MOBILE}px);

  padding: ${({ theme }) => theme.spacing(2)};

  -webkit-box-shadow: inset 0px 8px 9px -9px rgba(66, 68, 90, 1);
  -moz-box-shadow: inset 0px 8px 9px -9px rgba(66, 68, 90, 1);
  box-shadow: inset 0px 8px 9px -9px rgba(66, 68, 90, 1);

  ${({ theme }) => theme.breakpoints.up("lg")} {
    height: calc(100vh - ${TOPBAR_HEIGHT}px);
    padding: ${({ theme }) => theme.spacing(3)};

    -webkit-box-shadow: inset 8px 8px 9px -9px rgba(66, 68, 90, 1);
    -moz-box-shadow: inset 8px 8px 9px -9px rgba(66, 68, 90, 1);
    box-shadow: inset 8px 8px 9px -9px rgba(66, 68, 90, 1);
  }
`;
