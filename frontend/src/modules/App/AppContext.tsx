import { createContext } from "react";

import { UserToken } from "@src/common/helpers/userToken";

export interface AppContextType {
  userToken: UserToken | null;
}

const AppContext: React.Context<AppContextType> = createContext(
  {} as AppContextType,
);

export default AppContext;
