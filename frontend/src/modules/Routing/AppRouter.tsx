import React, { lazy } from "react";
import { Route, Routes } from "react-router-dom";

import routes from "@src/modules/Routing/routes";

import AppSkeleton from "@src/modules/App/AppSkeleton";
import RequireAuthed from "@src/modules/Routing/RouteTypes/RequireAuthed";
import RequirePublic from "@src/modules/Routing/RouteTypes/RequirePublic";
import Other from "@src/modules/Routing/RouteTypes/Other";

const mappedRoutes = routes.map((route, index) => {
  const Element = lazy(route.importComponent);
  const path = route.isNested ? `${route.path}/*` : route.path;

  switch (route.type) {
    case "authed":
      return (
        <Route
          key={index}
          path={path}
          element={<RequireAuthed title={route.title} element={<Element />} />}
        />
      );

    case "public":
      return (
        <Route
          key={index}
          path={path}
          element={<RequirePublic title={route.title} element={<Element />} />}
        />
      );

    case "other":
      return (
        <Route
          key={index}
          path={path}
          element={<Other title={route.title} element={<Element />} />}
        />
      );
  }
});

const AppRouter = () => (
  <Routes>
    <Route path="/" element={<AppSkeleton />}>
      {mappedRoutes}
    </Route>
  </Routes>
);

export default AppRouter;
