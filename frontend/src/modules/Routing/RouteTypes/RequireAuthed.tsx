import React, { FunctionComponent, Suspense, useEffect } from "react";
import { Navigate } from "react-router";

import routesPaths from "@src/modules/Routing/routesPaths";
import useAppContext from "@src/modules/App/hooks/useAppContext";
import FallbackSpinner from "@src/modules/Shared/components/FallbackSpinner";
import { RouteProps } from "@src/modules/Routing/RouteTypes/types";

const RequireAuthed: FunctionComponent<RouteProps> = ({ element, title }) => {
  // token means that user is logged in
  const hasToken = !!useAppContext().userToken;

  useEffect(() => {
    document.title = title ?? "Offblocks";
  }, [title]);

  if (!hasToken) {
    return (
      <Navigate
        to={{
          pathname: routesPaths.LANDING,
        }}
      />
    );
  }

  return <Suspense fallback={<FallbackSpinner />}>{element}</Suspense>;
};
export default RequireAuthed;
