import { FieldProps, getIn } from "formik";
import React from "react";

import { SelectProps } from "@mui/material";
import {
  SelectWrapper,
  StyledSelect,
  StyledTypography,
} from "@src/modules/Shared/components/Select/Select.components";

const Select: React.FunctionComponent<
  SelectProps & FieldProps & { title?: string }
> = (props) => {
  const isTouched = getIn(props.form.touched, props.field.name);
  const errorMessage = getIn(props.form.errors, props.field.name);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { error, field, form, children, ...rest } = props;

  return (
    <SelectWrapper>
      {props.title && (
        <StyledTypography variant="overline">{props.title}</StyledTypography>
      )}
      <StyledSelect
        error={error ?? Boolean(isTouched && errorMessage)}
        {...rest}
        {...field}
      >
        {children}
      </StyledSelect>
    </SelectWrapper>
  );
};

export default Select;
