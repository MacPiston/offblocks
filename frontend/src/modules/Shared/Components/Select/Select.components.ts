import styled from "styled-components";

import { Box, Select, Typography } from "@mui/material";

export const SelectWrapper = styled(Box)`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const StyledTypography = styled(Typography)`
  margin-left: ${({ theme }) => theme.spacing(1)};
`;

export const StyledSelect = styled(Select)``;
