import React from "react";

import {
  StyledCircularProgress,
  StyledBackdrop,
} from "@src/modules/Shared/components/FallbackSpinner/FallbackSpinner.components";

const FallbackSpinner = () => (
  <StyledBackdrop open>
    <StyledCircularProgress />
  </StyledBackdrop>
);

export default FallbackSpinner;
