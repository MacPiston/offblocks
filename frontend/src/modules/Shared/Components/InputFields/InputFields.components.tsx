import { styled, TextField, Typography } from "@mui/material";
import { Box } from "@mui/system";

export const TextFieldWrapper = styled(Box)<{ inputTextCentered?: boolean }>`
  width: 100%;
  display: flex;
  flex-direction: column;

  input {
    text-align: ${({ inputTextCentered }) =>
      inputTextCentered ? "center" : "left"};
  }
`;

export const StyledTextField = styled(TextField)``;

export const StyledTypography = styled(Typography)`
  margin-left: ${({ theme }) => theme.spacing(1)};
`;
