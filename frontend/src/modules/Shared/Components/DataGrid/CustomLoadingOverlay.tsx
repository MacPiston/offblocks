import React from "react";

interface Props {
  loadingMessage: string;
}

const CustomLoadingOverlay: React.FunctionComponent<Props> = ({
  loadingMessage,
}) => (
  <div
    className="ag-custom-loading-cell"
    style={{ paddingLeft: "10px", lineHeight: "25px" }}
  >
    <i className="fas fa-spinner fa-pulse" /> <span>{loadingMessage}</span>
  </div>
);

export default CustomLoadingOverlay;
