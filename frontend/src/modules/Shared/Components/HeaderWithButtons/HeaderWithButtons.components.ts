import { Box, styled } from "@mui/material";

export const HorizontalFlexWrapper = styled(Box)`
  display: flex;
  justify-content: space-between;
`;

export const HeaderButtonsWrapper = styled(Box)`
  display: flex;

  & > *:not(:first-child) {
    margin-left: ${({ theme }) => theme.spacing(1)};
  }
`;
