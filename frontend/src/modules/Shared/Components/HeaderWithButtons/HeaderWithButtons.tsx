import React from "react";
import { Box, Divider } from "@mui/material";

import {
  HeaderButtonsWrapper,
  HorizontalFlexWrapper,
} from "@src/modules/Shared/components/HeaderWithButtons/HeaderWithButtons.components";
import { H5 } from "@src/modules/Shared/components/Typography";

interface Props {
  title: string;
  buttons?: React.ReactNode;
}

const HeaderWithButtons: React.FunctionComponent<Props> = ({
  title,
  buttons,
}) => (
  <Box>
    <HorizontalFlexWrapper>
      <H5>{title}</H5>
      <HeaderButtonsWrapper>{buttons}</HeaderButtonsWrapper>
    </HorizontalFlexWrapper>
    <Divider sx={{ marginTop: 2, marginBottom: 2 }} />
  </Box>
);

export default HeaderWithButtons;
