import React from "react";
import styled from "styled-components";

import { CircularProgress } from "@mui/material";

const StyledCircularProgress = styled(CircularProgress)`
  color: ${({ theme }) => theme.palette.primary.main};
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translateX(-50%);
`;

const Spinner = () => <StyledCircularProgress />;

export default Spinner;
