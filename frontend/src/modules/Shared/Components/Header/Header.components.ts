import styled from "styled-components";

import { Box, Divider, Button } from "@mui/material";

export const HeaderWrapper = styled(Box)`
  display: flex;
  justify-content: space-between;
  padding: 0 ${({ theme }) => theme.spacing(4)};
`;

export const StyledDivider = styled(Divider)`
  margin: ${({ theme }) => theme.spacing(2)} 0;
`;

export const StyledButton = styled(Button)<{ $iconColor?: string }>`
  svg {
    width: 32px;
    height: auto;
    ${({ $iconColor }) => ($iconColor ? `color: ${$iconColor}` : null)}
  }
`;
