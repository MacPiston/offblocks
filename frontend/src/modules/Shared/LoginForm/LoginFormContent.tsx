import React from "react";
import { FormikProps, Field } from "formik";

import { LoginModel } from "@src/modules/Shared/LoginForm/useLogin";
import { Button, CircularProgress } from "@mui/material";
import TextField from "@src/modules/Shared/components/InputFields";
import {
  TextFieldsWrapper,
  StyledForm,
  LoginFormHeader,
  HorizontalItems,
} from "@src/modules/Shared/LoginForm/LoginFormContent.components";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";

type Props = FormikProps<LoginModel> & { title?: string };

const LoginFormContent: React.FunctionComponent<Props> = ({
  isSubmitting,
  title,
}) => {
  const { toRegister } = useRedirects();

  return (
    <StyledForm>
      {title && <LoginFormHeader variant="h4">Login</LoginFormHeader>}
      <TextFieldsWrapper>
        <Field
          name="email"
          title="Your email"
          placeholder="E-Mail address"
          type="email"
          component={TextField}
        />
        <Field
          name="password"
          title="Your password"
          placeholder="Password"
          type="password"
          component={TextField}
        />
      </TextFieldsWrapper>
      <HorizontalItems>
        <Button type="submit" variant="contained" size="large">
          {isSubmitting ? (
            <CircularProgress color="secondary" size={25} />
          ) : (
            "Login"
          )}
        </Button>
        <Button variant="outlined" onClick={toRegister}>
          Create account
        </Button>
      </HorizontalItems>
    </StyledForm>
  );
};

export default LoginFormContent;
