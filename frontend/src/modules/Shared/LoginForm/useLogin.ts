import { AxiosError, AxiosResponse } from "axios";
import { useMutation } from "react-query";

import { LoginResponse } from "@src/common/helpers/userToken";
import useAxios from "@src/common/axios/useAxios";
import { LOGIN_ENDPOINT } from "@src/config/baseUrls";

export interface LoginModel {
  email: string;
  password: string;
}

const useLogin = () => {
  const axios = useAxios();

  return useMutation<AxiosResponse<LoginResponse>, AxiosError, LoginModel>(
    async (values) => await axios.post(LOGIN_ENDPOINT, values),
  );
};

export default useLogin;
