import { useCallback } from "react";
import { useSnackbar } from "notistack";

const DEFAULT_AUTOHIDE = 4000;

const useNotifications = () => {
  const { enqueueSnackbar } = useSnackbar();

  const success = useCallback(
    (message: string) =>
      enqueueSnackbar(message, {
        variant: "success",
        preventDuplicate: true,
        autoHideDuration: DEFAULT_AUTOHIDE,
      }),
    [enqueueSnackbar],
  );

  const warning = useCallback(
    (message: string) =>
      enqueueSnackbar(message, {
        variant: "warning",
        preventDuplicate: true,
        autoHideDuration: DEFAULT_AUTOHIDE,
      }),
    [enqueueSnackbar],
  );

  const error = useCallback(
    (message: string) =>
      enqueueSnackbar(message, {
        variant: "error",
        preventDuplicate: true,
        autoHideDuration: DEFAULT_AUTOHIDE,
      }),
    [enqueueSnackbar],
  );

  return { success, warning, error };
};

export default useNotifications;
