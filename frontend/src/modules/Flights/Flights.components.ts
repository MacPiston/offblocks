import { Box, styled } from "@mui/material";

import ColoredFab from "@src/modules/Shared/components/ColoredFab";

export const FlightsOutletWrapper = styled(Box)`
  height: 100%;
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
`;

export const StyledFab = styled(ColoredFab)`
  margin: 0 auto;
  display: flex;
`;
