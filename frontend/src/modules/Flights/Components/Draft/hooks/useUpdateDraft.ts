import { AxiosError, AxiosResponse } from "axios";
import { useMutation } from "react-query";

import useAxios from "@src/common/axios/useAxios";

import { FLIGHT_DRAFT_ENDPOINT } from "@src/config/baseUrls";
import {
  DraftResponse,
  Landings,
  OperationalConditionTime,
  PathPoint,
  PilotFunctionTime,
  PilotTime,
  SyntheticTrainingDeviceSession,
} from "@src/modules/Flights/Components/Drafts/hooks/useDrafts";
import { FlightFormModel } from "@src/modules/Flights/Components/shared/model";
import { timeStringToMinutes } from "@src/common/helpers/date";

export interface UpdateDraftModel {
  departureDate?: string;
  departurePoint?: PathPoint;
  // pathPoints?: PathPoint[];
  arrivalDate?: string;
  arrivalPoint?: PathPoint;

  aircraftId?: string;
  pic?: string;
  landings?: Landings;

  pilotTime?: PilotTime<number>;
  pft?: PilotFunctionTime<number>;
  oct?: OperationalConditionTime<number>;
  ifrApproaches?: number;

  stds?: SyntheticTrainingDeviceSession<number>;
  remarks?: string;
}

const useUpdateDraft = (
  draftId: string,
  onSuccess?: () => void,
  onError?: () => void,
) => {
  const axios = useAxios();

  const mutation = useMutation<
    AxiosResponse<DraftResponse>,
    AxiosError,
    UpdateDraftModel
  >(
    async (data) =>
      await axios.put(`${FLIGHT_DRAFT_ENDPOINT}/${draftId}`, data),
  );

  const handleUpdate = (values: FlightFormModel) => {
    const mutationValues: UpdateDraftModel = {
      departureDate: values.departureDate,
      departurePoint: { ...values.departurePoint, type: "ARPT" },
      arrivalDate: values.arrivalDate,
      arrivalPoint: { ...values.arrivalPoint, type: "ARPT" },

      aircraftId: values.aircraftId,
      pic: values.pic,

      landings: values.landings,

      pilotTime: {
        spse: timeStringToMinutes(values.pilotTime?.spse),
        spme: timeStringToMinutes(values.pilotTime?.spme),
        mp: timeStringToMinutes(values.pilotTime?.mp),
      },
      pft: {
        pic: timeStringToMinutes(values.pft?.pic),
        coPilot: timeStringToMinutes(values.pft?.coPilot),
        dual: timeStringToMinutes(values.pft?.dual),
        instructor: timeStringToMinutes(values.pft?.instructor),
      },
      oct: {
        ifr: timeStringToMinutes(values.oct?.ifr),
        night: timeStringToMinutes(values.oct?.night),
      },
      stds: {
        date: values.stds?.date,
        type: values.stds?.type,
        totalTime: timeStringToMinutes(values.stds?.totalTime),
      },
      ifrApproaches: values.ifrApproaches,
      remarks: values.remarks,
    };

    mutation.mutate(mutationValues, { onError, onSuccess });
  };

  return handleUpdate;
};

export default useUpdateDraft;
