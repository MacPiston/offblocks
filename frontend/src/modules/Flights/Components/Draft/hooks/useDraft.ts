import { AxiosError, AxiosResponse } from "axios";
import { useQuery } from "react-query";

import useAxios from "@src/common/axios/useAxios";

import { FLIGHT_DRAFT_ENDPOINT } from "@src/config/baseUrls";
import { DraftResponse } from "@src/modules/Flights/Components/Drafts/hooks/useDrafts";

const useDraft = (draftId: string) => {
  const axios = useAxios();

  return useQuery<AxiosResponse<DraftResponse>, AxiosError>(
    "fetch-draft",
    async () => await axios.get(`${FLIGHT_DRAFT_ENDPOINT}/${draftId}`),
  );
};

export default useDraft;
