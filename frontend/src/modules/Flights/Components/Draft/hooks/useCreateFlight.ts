import useAxios from "@src/common/axios/useAxios";
import { FLIGHT_ENDPOINT } from "@src/config/baseUrls";
import { FlightResponse } from "@src/modules/Flights/Components/Flights/hooks/useFlights";
import { AxiosError, AxiosResponse } from "axios";
import { useMutation } from "react-query";

const useCreateFlight = (draftId: string) => {
  const axios = useAxios();

  return useMutation<AxiosResponse<FlightResponse>, AxiosError>(
    "create-flight",
    async () => await axios.post(`${FLIGHT_ENDPOINT}/create/${draftId}`),
  );
};

export default useCreateFlight;
