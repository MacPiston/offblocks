import { FlightFormModel } from "@src/modules/Flights/Components/shared/model";
import * as Yup from "yup";

// VALIDATORS

const dateTimeValidator = Yup.string().matches(
  /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/g,
  "Invalid format - ${path}!",
);

const timeFieldValidator = Yup.string().matches(
  /(\d{2}):(\d{2})|^$/g,
  "Invalid format - ${path}!",
);

export const pathPointValidatorOptional = Yup.object().shape({
  name: Yup.string().max(16).optional(),
  icaoName: Yup.string().length(4, "4 characters allowed").optional(),
  description: Yup.string().max(24).optional(),
});

export const pathPointValidatorRequired = Yup.object().shape({
  name: Yup.string().max(16).optional(),
  icaoName: Yup.string().length(4, "4 characters allowed").required(),
  description: Yup.string().max(24).optional(),
});

export const timeFieldValidatorOptional = timeFieldValidator.optional();

export const dateTimeFieldValidatorOptional = dateTimeValidator.optional();

export const timeFieldValidatorRequired = timeFieldValidator.required();

export const dateTimeFieldValidatorRequired = dateTimeValidator.required();

const landingsValidator = Yup.object().shape({
  daytime: Yup.number().min(0, "Must be >= than 0").optional(),
  nighttime: Yup.number().min(0, "Must be >= than 0").optional(),
});

const pilotTimeValidator = Yup.object().shape({
  spse: timeFieldValidatorOptional,
  spme: timeFieldValidatorOptional,
  mp: timeFieldValidatorOptional,
});

const pftValidator = Yup.object().shape({
  pic: timeFieldValidatorOptional,
  coPilot: timeFieldValidatorOptional,
  dual: timeFieldValidatorOptional,
  instructor: timeFieldValidatorOptional,
});

const octValidator = Yup.object().shape({
  ifr: timeFieldValidatorOptional,
  night: timeFieldValidatorOptional,
});

const ifrApproachesValidator = Yup.number().min(0, "Must be >= than 0");

// SCHEMAS

const formValidationSchema = Yup.object().shape({
  departureDate: dateTimeFieldValidatorOptional,
  departurePoint: pathPointValidatorOptional,
  arrivalDate: dateTimeFieldValidatorOptional,
  arrivalPoint: pathPointValidatorOptional,

  aircraftId: Yup.string().optional(),
  pic: Yup.string().max(16).optional(),

  landings: landingsValidator.optional(),

  pilotTime: pilotTimeValidator.optional(),
  pft: pftValidator.optional(),
  oct: octValidator.optional(),
  ifrApproaches: ifrApproachesValidator.required(),
});

export const flightValidationSchema = Yup.object().shape({
  departureDate: dateTimeFieldValidatorRequired,
  departurePoint: pathPointValidatorRequired,
  arrivalDate: dateTimeFieldValidatorRequired,
  arrivalPoint: pathPointValidatorRequired,

  aircraftId: Yup.string().required(),
  pic: Yup.string().max(16).required(),

  landings: landingsValidator.required(),

  pilotTime: pilotTimeValidator.required(),
  pft: pftValidator.required(),
  oct: octValidator.optional(),
  ifrApproaches: ifrApproachesValidator.optional(),
});

// VALIDATION FUNCTIONS

export const validateBeforeFlight = (values: FlightFormModel): boolean => {
  let error;
  try {
    flightValidationSchema.validateSync(values);
  } catch (e: unknown) {
    error = e;
    // eslint-disable-next-line no-console
    // console.log(e);
  }

  return !!error;
};

export default formValidationSchema;
