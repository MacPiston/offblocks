import { Form } from "formik";
import styled, { css } from "styled-components";

import { H6 } from "@src/modules/Shared/components/Typography";
import { Box, Button } from "@mui/material";
import TextField from "@src/modules/Shared/components/InputFields";
import Select from "@src/modules/Shared/components/Select";

export const StyledForm = styled(Form)`
  & > hr {
    margin: ${({ theme }) => theme.spacing(3)} 0;
  }
`;

export const FieldDescription = styled(H6)``;

export const FormRow = styled(Box)<{ width?: string; elementSpacing?: number }>`
  ${({ width }) =>
    width
      ? `width: ${width}; margin-left: auto; margin-right: auto;`
      : "width: 100%;"}
  display: flex;
  justify-content: center;
  align-items: center;

  & > hr {
    margin: 0 ${({ theme }) => theme.spacing(2)};
  }

  ${({ elementSpacing }) =>
    elementSpacing &&
    css`
      & > div:not(:first-child) {
        margin-left: ${({ theme }) => theme.spacing(elementSpacing)};
      }
    `}
`;

export const FieldRow = styled(Box)<{
  vertical?: boolean;
  flex?: number;
  divWidth?: number;
}>`
  width: 100%;
  display: flex;
  ${({ flex }) => flex && `flex: ${flex};`}
  ${({ vertical }) => vertical && "flex-direction: column;"}
  align-items: center;

  & > h6 {
    flex: 3;
    text-align: center;
  }

  & > div {
    flex: 5;
    ${({ vertical, divWidth }) =>
      vertical && divWidth && `width: ${divWidth}%;`}
    margin-left: ${({ theme, vertical }) => !vertical && theme.spacing(2)};
  }
`;

export const SmallTextField = styled(TextField).attrs({ size: "small" })``;

export const SmallSelect = styled(Select).attrs({ size: "small" })`
  margin-bottom: 1.6rem;
`;

export const SubmitButton = styled(Button).attrs({
  color: "success",
  variant: "contained",
})`
  display: flex;
  & > span {
    margin-left: ${({ theme }) => theme.spacing(2)};
  }
`;

export const CreateFlightButton = styled(Button).attrs({
  color: "secondary",
  variant: "outlined",
})``;
