import React, { useRef } from "react";
import { Formik, FormikProps } from "formik";

import useAircrafts from "@src/modules/Aircrafts/hooks/useAircrafts";
import useDraft from "@src/modules/Flights/Components/Draft/hooks/useDraft";
import useNotifications from "@src/modules/Shared/hooks/useNotifications";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import useParseDraftData from "@src/modules/Flights/Components/Draft/hooks/useParseDraftData";
import useUpdateDraft from "@src/modules/Flights/Components/Draft/hooks/useUpdateDraft";
import useCreateFlight from "@src/modules/Flights/Components/Draft/hooks/useCreateFlight";

import formValidationSchema from "@src/modules/Flights/Components/Draft/DraftForm/validation";
import { FlightFormModel } from "@src/modules/Flights/Components/shared/model";

import Spinner from "@src/modules/Shared/components/Spinner";
import FlightFormContent from "@src/modules/Flights/Components/shared/FormContent/FlightFormContent";

const DraftForm: React.FunctionComponent<{ draftId: string }> = ({
  draftId,
}) => {
  const formikRef = useRef<FormikProps<FlightFormModel>>(null);

  const { error, success } = useNotifications();
  const { toDrafts, toFlights } = useRedirects();

  const {
    data: aircrafts,
    isLoading: aircraftsLoading,
    error: aircraftsError,
  } = useAircrafts();
  const {
    data: draft,
    isLoading: draftLoading,
    error: draftError,
    refetch,
    isRefetching,
  } = useDraft(draftId);
  const draftData = useParseDraftData(draft?.data);
  const createFlightMutation = useCreateFlight(draftId);
  const updateDraft = useUpdateDraft(
    draftId,
    async () => {
      formikRef.current?.setSubmitting(false);
      success("Draft saved!");
      await refetch();
    },
    () => {
      formikRef.current?.setSubmitting(false);
      error("Error saving");
    },
  );

  const onCreateFlight = async () => {
    await formikRef.current?.submitForm();
    createFlightMutation.mutate(undefined, {
      onSuccess: () => {
        success("Flight created!");
        toFlights();
      },
      onError: () => {
        error("Error creating flight");
      },
    });
  };

  if (aircraftsLoading || draftLoading || isRefetching) return <Spinner />;

  if (aircraftsError || draftError) {
    error("Error fetching draft data");
    toDrafts();
    return null;
  }

  return (
    <Formik
      initialValues={{ ...draftData! }}
      validationSchema={formValidationSchema}
      onSubmit={updateDraft}
      innerRef={formikRef}
    >
      {(formProps) => (
        <FlightFormContent
          withCreateButton
          aircrafts={aircrafts!.data}
          onSaveFlight={onCreateFlight}
          {...formProps}
        />
      )}
    </Formik>
  );
};

export default DraftForm;
