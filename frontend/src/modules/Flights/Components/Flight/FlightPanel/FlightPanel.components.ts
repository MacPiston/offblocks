import styled from "styled-components";

import { Box, css } from "@mui/material";
import { H6, Sub1Bold } from "@src/modules/Shared/components/Typography";

export const PanelWrapper = styled(Box)`
  flex: 1;
  display: flex;
  flex-direction: column;

  height: 100%;
  width: 80%;
  margin: 0 auto;
  justify-content: center;

  & > hr {
    margin: ${({ theme }) => theme.spacing(3)} 0;
  }

  .detail {
    font-weight: ${({ theme }) => theme.typography.fontWeightBold};
  }
`;

export const PanelRow = styled(Box)<{ spaceContent?: boolean }>`
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  align-items: center;

  & > hr {
    margin: 0 ${({ theme }) => theme.spacing(2)};
  }

  ${({ spaceContent }) =>
    spaceContent &&
    css`
      & > div:not(:first-child) {
        margin-left: ${({ theme }) => theme.spacing(2)};
      }
    `}
`;

export const VerticalWrapper = styled(Box)<{ centerText?: boolean }>`
  ${({ centerText }) => centerText && "text-align: center;"}

  & > *:not(:first-child) {
    margin-top: ${({ theme }) => theme.spacing(2)};
  }
`;

export const InformationHeader = styled(H6)<{ $hasLink?: boolean }>`
  text-align: center;
  ${({ $hasLink }) => ($hasLink ? "cursor: pointer;" : null)}
`;

export const SmallInformationHeader = styled(Sub1Bold)``;
