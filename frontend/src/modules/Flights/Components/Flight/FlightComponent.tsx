import React, { useState } from "react";
import { useParams } from "react-router-dom";

import useNotifications from "@src/modules/Shared/hooks/useNotifications";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import useFlight from "@src/modules/Flights/Components/Flight/hooks/useFlight";

import {
  FlightComponentWrapper,
  StyledButton,
} from "@src/modules/Flights/Components/Flight/FlightComponent.components";
import Spinner from "@src/modules/Shared/components/Spinner";
import FlightPanel from "@src/modules/Flights/Components/Flight/FlightPanel";
import EditFlight from "@src/modules/Flights/Components/Flight/EditFlight";
import HeaderWithButtons from "@src/modules/Shared/components/HeaderWithButtons";

const FlightComponent: React.FunctionComponent = () => {
  const { flightId } = useParams();
  const {
    data,
    isLoading,
    isRefetching,
    error: flightError,
    refetch,
  } = useFlight(flightId ?? "");
  const [isEditing, setIsEditing] = useState(false);
  const { error } = useNotifications();
  const { toFlights } = useRedirects();

  if (!flightId) {
    toFlights();
    return null;
  }

  if (isLoading || isRefetching) return <Spinner />;

  if (flightError) {
    error("Error fetching data");
    toFlights();
    return null;
  }

  return (
    <FlightComponentWrapper>
      <HeaderWithButtons
        title="Flight"
        buttons={
          <StyledButton
            disabled={isLoading}
            onClick={() => setIsEditing((prev) => !prev)}
          >
            Edit
          </StyledButton>
        }
      />

      {isLoading ? (
        <Spinner />
      ) : isEditing ? (
        <EditFlight
          refetch={async () => {
            setIsEditing(false);
            await refetch();
          }}
          flightData={data!.data}
        />
      ) : (
        <FlightPanel data={data!.data} />
      )}
    </FlightComponentWrapper>
  );
};

export default FlightComponent;
