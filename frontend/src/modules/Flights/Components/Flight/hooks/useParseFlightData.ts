import { minutesToTimeString } from "@src/common/helpers/date";
import { FlightFormModel } from "@src/modules/Flights/Components/shared/model";
import { FlightResponse } from "@src/modules/Flights/Components/Flights/hooks/useFlights";

const useParseFlightData = (
  data?: FlightResponse,
): FlightFormModel | undefined => {
  if (!data) return undefined;

  const parsedObject: FlightFormModel = {
    departureDate: data.departureDate?.substring(0, 16),
    departurePoint: {
      icaoName: data.departurePoint?.icaoName || "",
      type: "ARPT",
    },
    arrivalDate: data.arrivalDate?.substring(0, 16),
    arrivalPoint: {
      icaoName: data.arrivalPoint?.icaoName || "",
      type: "ARPT",
    },

    aircraftId: data.aircraft._id || "",
    pic: data.pic || "",
    landings: data.landings || { daytime: 0, nighttime: 0 },

    pilotTime: {
      spse: minutesToTimeString(data.pilotTime?.spse),
      spme: minutesToTimeString(data.pilotTime?.spme),
      mp: minutesToTimeString(data.pilotTime?.mp),
    },
    pft: {
      pic: minutesToTimeString(data.pft?.pic),
      coPilot: minutesToTimeString(data.pft?.coPilot),
      dual: minutesToTimeString(data.pft?.dual),
      instructor: minutesToTimeString(data.pft?.instructor),
    },
    oct: {
      night: minutesToTimeString(data.oct?.night),
      ifr: minutesToTimeString(data.oct?.ifr),
    },
    stds: {
      date: data.stds?.date?.substring(0, 10),
      type: data.stds?.type || "",
      totalTime: minutesToTimeString(data.stds?.totalTime),
    },
    ifrApproaches: data.ifrApproaches || 0,
    remarks: data.remarks || "",
  };

  return parsedObject;
};

export default useParseFlightData;
