import React, { useState } from "react";
import { RowSelectedEvent } from "ag-grid-community";

import useDrafts from "@src/modules/Flights/Components/Drafts/hooks/useDrafts";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import useParseDraftsData from "@src/modules/Flights/Components/Drafts/hooks/useParseDraftsData";
import useDeleteDraft from "@src/modules/Flights/Components/Drafts/hooks/useDeleteDraft";
import useNotifications from "@src/modules/Shared/hooks/useNotifications";

import draftsColumnDefs from "@src/modules/Flights/Components/Drafts/dataGridConfig/columns";

import {
  DraftsComponentWrapper,
  StyledButton,
} from "@src/modules/Flights/Components/Drafts/DraftsComponent.components";
import DataGrid from "@src/modules/Shared/components/DataGrid/DataGrid";
import CustomLoadingOverlay from "@src/modules/Shared/components/DataGrid/CustomLoadingOverlay";
import DraftRow from "@src/modules/Flights/Components/Drafts/dataGridConfig/row";
import HeaderWithButtons from "@src/modules/Shared/components/HeaderWithButtons";

const DraftsComponent: React.FunctionComponent = () => {
  const [selectedDraftId, setSelectedDraftId] = useState<string | undefined>(
    undefined,
  );
  const { data, refetch } = useDrafts();
  const drafts = useParseDraftsData(data?.data);
  const deleteMutation = useDeleteDraft(selectedDraftId);
  const { toDraft } = useRedirects();
  const { error } = useNotifications();

  const handleRowSelected = (e: RowSelectedEvent) => {
    const id = (e.data as DraftRow).id;
    if (id !== selectedDraftId) setSelectedDraftId(id);
  };

  const handleFinishEditing = () => {
    if (!selectedDraftId) return;
    toDraft(selectedDraftId);
  };

  const handleDelete = () => {
    if (!selectedDraftId) return;
    deleteMutation.mutate(undefined, {
      onSuccess: async () => {
        await refetch();
      },
      onError: () => {
        error("Error occurred");
      },
    });
  };

  return (
    <DraftsComponentWrapper>
      <HeaderWithButtons
        title="Drafts"
        buttons={
          <>
            <StyledButton
              color="error"
              disabled={!selectedDraftId}
              onClick={handleDelete}
            >
              Delete
            </StyledButton>
            <StyledButton
              disabled={!selectedDraftId}
              onClick={handleFinishEditing}
            >
              Finish editing
            </StyledButton>
          </>
        }
      />
      <DataGrid
        columns={draftsColumnDefs}
        rows={drafts}
        frameworkComponents={{
          customLoadingOverlay: CustomLoadingOverlay,
        }}
        loadingOverlayComponent={"customLoadingOverlay"}
        loadingOverlayComponentParams={{
          loadingMessage: "Fetching your drafts...",
        }}
        rowSelection="single"
        onRowSelected={handleRowSelected}
        onFirstDataRendered={(event) => event.columnApi.autoSizeAllColumns()}
      />
    </DraftsComponentWrapper>
  );
};

export default DraftsComponent;
