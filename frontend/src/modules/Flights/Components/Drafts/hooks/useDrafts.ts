import { useQuery } from "react-query";
import { AxiosError, AxiosResponse } from "axios";

import useAxios from "@src/common/axios/useAxios";
import { FLIGHT_DRAFT_ENDPOINT } from "@src/config/baseUrls";

export interface PathPoint {
  name?: string;
  icaoName?: string;
  type: "ARPT" | "WAYP";
  description?: string;
  point?: { coordinates: number[]; type: string };
}

export interface PilotTime<T extends string | number> {
  spse?: T;
  spme?: T;
  mp?: T;
}

export interface Landings {
  daytime?: number;
  nighttime?: number;
}

export interface OperationalConditionTime<T extends string | number> {
  night?: T;
  ifr?: T;
}

export interface PilotFunctionTime<T extends string | number> {
  pic?: T;
  coPilot?: T;
  dual?: T;
  instructor?: T;
}

export interface SyntheticTrainingDeviceSession<T> {
  date?: string;
  type?: string;
  totalTime?: T;
}

interface DraftModel {
  createdOn: string;
  departureDate?: string;
  departurePoint?: PathPoint;
  // pathPoints?: PathPoint[];
  arrivalDate?: string;
  arrivalPoint?: PathPoint;

  aircraftId?: string;
  pic?: string;
  landings?: Landings;

  pilotTime?: PilotTime<number>;
  pft?: PilotFunctionTime<number>;
  oct?: OperationalConditionTime<number>;
  ifrApproaches?: number;

  stds?: SyntheticTrainingDeviceSession<number>;
  remarks?: string;
}

export interface DraftResponse extends DraftModel {
  _id: string;
}

const useDrafts = () => {
  const axios = useAxios();

  return useQuery<AxiosResponse<DraftResponse[]>, AxiosError>(
    "fetch-drafts",
    async () => await axios.get(`${FLIGHT_DRAFT_ENDPOINT}/all`),
  );
};

export default useDrafts;
