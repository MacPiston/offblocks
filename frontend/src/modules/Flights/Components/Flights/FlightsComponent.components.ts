import styled from "styled-components";

import ColoredFab from "@src/modules/Shared/components/ColoredFab";
import { Box, Button } from "@mui/material";

export const FlightsTabPanel = styled(Box)`
  flex: 1;
  display: flex;
  flex-direction: column;
  position: relative;
  width: 100%;
  height: 100%;
  padding-top: 0;
`;

export const FlightsTabFab = styled(ColoredFab)`
  position: absolute;
  right: 0;
  bottom: 0;
`;

export const ButtonsContainer = styled(Box)`
  display: flex;
  width: 100%;
  margin-bottom: ${({ theme }) => theme.spacing(2)};
  justify-content: flex-end;

  & > *:not(:first-child) {
    margin-left: ${({ theme }) => theme.spacing(2)};
  }
`;

export const StyledButton = styled(Button).attrs({ variant: "contained" })``;
