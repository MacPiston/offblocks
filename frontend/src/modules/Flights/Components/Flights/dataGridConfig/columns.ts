import FlightRow from "@src/modules/Flights/Components/Flights/dataGridConfig/row";
import { ExtendedColDef } from "@src/modules/Shared/components/DataGrid/defaults";

const fieldName = (name: keyof FlightRow) => name;

const flightsColumnDefs: ExtendedColDef[] = [
  {
    headerName: "Departure",
    nestedCols: [
      {
        headerName: "Dep. date",
        field: fieldName("departureDate"),
        editable: false,
        sortable: true,
      },
      {
        headerName: "Dep. place",
        field: fieldName("departurePlace"),
        editable: false,
        sortable: true,
      },
    ],
  },
  {
    headerName: "Arrival",
    nestedCols: [
      {
        headerName: "Arr. date",
        field: fieldName("arrivalDate"),
        editable: false,
        sortable: true,
      },
      {
        headerName: "Arr. place",
        field: fieldName("arrivalPlace"),
        editable: false,
        sortable: true,
      },
    ],
  },
  {
    headerName: "Aircraft",
    nestedCols: [
      {
        headerName: "Registration",
        field: fieldName("registration"),
        editable: false,
        sortable: true,
      },
      {
        headerName: "Type",
        field: fieldName("type"),
        columnGroupShow: "open",
        editable: false,
        sortable: true,
      },
    ],
  },
  {
    headerName: "PIC",
    field: fieldName("pic"),
    editable: false,
    sortable: true,
  },
  {
    headerName: "Landings",
    editable: false,
    nestedCols: [
      {
        headerName: "Total",
        field: fieldName("landings_total"),
        editable: false,
        sortable: true,
      },
      {
        headerName: "Day",
        field: fieldName("landings_day"),
        columnGroupShow: "open",
        editable: false,
        sortable: true,
      },
      {
        headerName: "Night",
        field: fieldName("landings_night"),
        columnGroupShow: "open",
        editable: false,
        sortable: true,
      },
    ],
  },
  {
    headerName: "SP Time",
    editable: false,
    nestedCols: [
      {
        headerName: "Total",
        field: fieldName("pilotTime_SP_total"),
        editable: false,
        sortable: true,
      },
      {
        headerName: "SE",
        field: fieldName("pilotTime_SE"),
        columnGroupShow: "open",
        editable: false,
        sortable: true,
      },
      {
        headerName: "ME",
        field: fieldName("pilotTime_ME"),
        columnGroupShow: "open",
        editable: false,
        sortable: true,
      },
    ],
  },
  {
    headerName: "MP Time",
    editable: false,
    field: fieldName("pilotTime_MP"),
    sortable: true,
  },
  // {
  //   headerName: "Total time",
  //   field: fieldName("time_total"),
  // },
  {
    headerName: "PFT",
    editable: false,
    nestedCols: [
      {
        headerName: "Total",
        field: fieldName("pft_total"),
        editable: false,
        sortable: true,
      },
      {
        headerName: "PIC",
        field: fieldName("pft_pic"),
        columnGroupShow: "open",
        editable: false,
        sortable: true,
      },
      {
        headerName: "Co-Pilot",
        field: fieldName("pft_copilot"),
        columnGroupShow: "open",
        editable: false,
        sortable: true,
      },
      {
        headerName: "Dual",
        field: fieldName("pft_dual"),
        columnGroupShow: "open",
        editable: false,
        sortable: true,
      },
      {
        headerName: "Instructor",
        field: fieldName("pft_instructor"),
        columnGroupShow: "open",
        editable: false,
        sortable: true,
      },
    ],
  },

  {
    headerName: "OCT",
    editable: false,
    nestedCols: [
      {
        headerName: "Total",
        field: fieldName("oct_total"),
        editable: false,
        sortable: true,
      },
      {
        headerName: "Night",
        field: fieldName("oct_night"),
        columnGroupShow: "open",
        editable: false,
        sortable: true,
      },
      {
        headerName: "IFR",
        field: fieldName("oct_ifr"),
        columnGroupShow: "open",
        editable: false,
        sortable: true,
      },
    ],
  },
  {
    headerName: "IFR Approaches",
    field: fieldName("ifrApproaches"),
    editable: false,
    sortable: true,
  },
  {
    headerName: "STDS",
    nestedCols: [
      {
        headerName: "Total",
        field: fieldName("stds_time"),
      },
      {
        headerName: "Date",
        field: fieldName("stds_date"),
        columnGroupShow: "open",
      },
      {
        headerName: "Type",
        field: fieldName("stds_type"),
        columnGroupShow: "open",
      },
    ],
  },
  {
    headerName: "Remarks",
    field: fieldName("remarks"),
  },
];

export default flightsColumnDefs;
