import React from "react";
import { Field } from "formik";
import { Divider, MenuItem } from "@mui/material";

import { AircraftModel } from "@src/modules/Aircrafts/hooks/useAircraft";

import {
  FieldDescription,
  FieldRow,
  FormRow,
  SmallTextField,
  SmallSelect,
} from "@src/modules/Flights/Components/Draft/DraftForm/DraftForm.components";

interface Props {
  aircrafts: AircraftModel[];
}

const FirstRow: React.FunctionComponent<Props> = ({ aircrafts }) => (
  <FormRow>
    <div style={{ width: "100%", flex: "6" }}>
      <FieldRow divWidth={90}>
        <FieldDescription>Departure</FieldDescription>
        <Field
          title="Dep. airfield"
          name="departurePoint.icaoName"
          placeholder="ICAO"
          type="text"
          component={SmallTextField}
          inputProps={{ style: { textTransform: "uppercase" } }}
        />
        <Field
          title="Departure time"
          name="departureDate"
          type="datetime-local"
          component={SmallTextField}
        />
      </FieldRow>
      <FieldRow>
        <FieldDescription>Arrival</FieldDescription>
        <Field
          title="Arr. airfield"
          name="arrivalPoint.icaoName"
          placeholder="ICAO"
          type="text"
          component={SmallTextField}
          inputProps={{ style: { textTransform: "uppercase" } }}
        />
        <Field
          title="Arrival time"
          name="arrivalDate"
          type="datetime-local"
          component={SmallTextField}
        />
      </FieldRow>
    </div>

    <Divider orientation="vertical" variant="middle" flexItem />

    <FieldRow vertical flex={3} divWidth={75}>
      <FieldDescription>Aircraft</FieldDescription>
      <Field
        title="Select aircraft"
        name="aircraftId"
        placeholder="Choose..."
        type="text"
        component={SmallSelect}
      >
        {aircrafts.length !== 0 ? (
          aircrafts.map((a) => (
            <MenuItem key={a._id} value={a._id}>
              {`${a.name} - ${a.type}`}
            </MenuItem>
          ))
        ) : (
          <MenuItem key="empty" value="">
            No aircrafts
          </MenuItem>
        )}
      </Field>
    </FieldRow>
  </FormRow>
);

export default FirstRow;
