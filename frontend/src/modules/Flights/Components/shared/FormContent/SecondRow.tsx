import React from "react";
import { Field } from "formik";

import {
  FieldDescription,
  FieldRow,
  FormRow,
  SmallTextField,
} from "@src/modules/Flights/Components/Draft/DraftForm/DraftForm.components";
import { Divider } from "@mui/material";

const SecondRow: React.FunctionComponent = () => (
  <FormRow width="80%">
    <FieldRow vertical flex={4}>
      <FieldDescription>Single Pilot Time</FieldDescription>
      <FieldRow>
        <Field
          title="SE"
          name="pilotTime.spse"
          type="time"
          component={SmallTextField}
        />
        <Field
          title="ME"
          name="pilotTime.spme"
          type="time"
          component={SmallTextField}
        />
      </FieldRow>
    </FieldRow>

    <Divider orientation="vertical" variant="middle" flexItem />

    <FieldRow vertical flex={2}>
      <FieldDescription>Multi Pilot Time</FieldDescription>
      <Field
        title="&nbsp;"
        name="pilotTime.mp"
        type="time"
        component={SmallTextField}
      />
    </FieldRow>

    <Divider orientation="vertical" variant="middle" flexItem />

    <FieldRow vertical flex={2}>
      <FieldDescription>PIC</FieldDescription>
      <Field
        title="PIC Name"
        name="pic"
        type="text"
        component={SmallTextField}
      />
    </FieldRow>
  </FormRow>
);

export default SecondRow;
