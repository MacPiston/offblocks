import React from "react";
import { Field } from "formik";

import {
  FieldDescription,
  FieldRow,
  FormRow,
  SmallTextField,
} from "@src/modules/Flights/Components/Draft/DraftForm/DraftForm.components";
import { Divider } from "@mui/material";

const FifthRow = () => (
  <FormRow>
    <FieldRow vertical flex={4}>
      <FieldDescription>Synthetic training devices session</FieldDescription>
      <FieldRow>
        <Field
          title="Date"
          name="stds.date"
          type="date"
          component={SmallTextField}
        />
        <Field
          title="Type"
          name="stds.type"
          type="text"
          component={SmallTextField}
        />
        <Field
          title="Time"
          name="stds.totalTime"
          type="time"
          component={SmallTextField}
        />
      </FieldRow>
    </FieldRow>

    <Divider orientation="vertical" variant="middle" flexItem />

    <FieldRow vertical flex={3} divWidth={90}>
      <FieldDescription>Remarks</FieldDescription>
      <Field
        name="remarks"
        title="Your remarks"
        placeholder="Enter text..."
        type="text"
        multiline
        minRows={3}
        component={SmallTextField}
      />
    </FieldRow>
  </FormRow>
);

export default FifthRow;
