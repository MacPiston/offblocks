import routesPaths from "@src/modules/Routing/routesPaths";
import { SubRouteProps } from "@src/modules/Shared/types";

export const flightsRouting: SubRouteProps[] = [
  {
    label: "Flights",
    path: routesPaths.flightsPaths.LIST,
    importComponent: () => import("@src/modules/Flights/Components/Flights"),
  },
  {
    label: "Flight",
    path: routesPaths.flightsPaths.FLIGHT,
    importComponent: () => import("@src/modules/Flights/Components/Flight"),
  },
  {
    label: "Drafts",
    path: routesPaths.flightsPaths.DRAFTS_LIST,
    importComponent: () => import("@src/modules/Flights/Components/Drafts"),
  },
  {
    label: "Draft",
    path: routesPaths.flightsPaths.DRAFT,
    importComponent: () => import("@src/modules/Flights/Components/Draft"),
  },
];
