import { useQuery } from "react-query";
import { AxiosError, AxiosResponse } from "axios";

import useAxios from "@src/common/axios/useAxios";
import { USER_ENDPOINT } from "@src/config/baseUrls";

interface SummaryResponse {
  aircraftsNumber: number;
  flightsNumber: number;
  latestDraftId: string;
  latestFlightId: string;
  currentMonthMinutes: number;
  totalMinutes: number;
}

const useSummary = () => {
  const axios = useAxios();

  return useQuery<AxiosResponse<SummaryResponse>, AxiosError>(
    "fetch-summary",
    async () => await axios.get(`${USER_ENDPOINT}/summary`),
  );
};

export default useSummary;
