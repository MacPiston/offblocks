import styled from "styled-components";

import { Box, Button } from "@mui/material";

export const ShortcutsContainer = styled(Box)`
  margin: 0 auto;
  display: grid;
  grid-template-columns: repeat(2, minmax(80px, 250px));

  gap: ${({ theme }) => theme.spacing(1.5)};

  & > button:first-child {
    border-top-left-radius: 12px;
  }

  & > button:nth-child(2) {
    border-top-right-radius: 12px;
  }

  & > button:nth-last-child(2) {
    border-bottom-left-radius: 12px;
  }

  & > button:last-child {
    border-bottom-right-radius: 12px;
  }
`;

export const ShortcutButton = styled(Button).attrs({
  size: "large",
})``;
