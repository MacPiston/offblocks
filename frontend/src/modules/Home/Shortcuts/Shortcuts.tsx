import React from "react";

import { ShortcutsContainer } from "@src/modules/Home/Shortcuts/Shortcuts.components";

const Shortcuts: React.FunctionComponent = ({ children }) => (
  <ShortcutsContainer>{children}</ShortcutsContainer>
);

export default Shortcuts;
