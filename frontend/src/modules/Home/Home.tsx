import React from "react";
import { Divider } from "@mui/material";
import { minutesToHours } from "date-fns";

import useNewDraftId from "@src/modules/Shared/hooks/useNewDraftId";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import useNotifications from "@src/modules/Shared/hooks/useNotifications";
import useSummary from "@src/modules/Home/hooks/useSummary";

import {
  HeaderWrapper,
  HomeWrapper,
  StatsWrapper,
  VerticalWrapper,
} from "@src/modules/Home/Home.components";
import { H3, H5, H6 } from "@src/modules/Shared/components/Typography";
import StatCard from "@src/modules/Home/StatCard";
import {
  ShortcutButton,
  ShortcutsContainer,
} from "@src/modules/Home/Shortcuts/Shortcuts.components";

const Home = () => {
  const {
    toDraft,
    toDrafts,
    toFlights,
    toAircrafts,
    toFlight,
  } = useRedirects();
  const { error } = useNotifications();

  const {
    data: summaryData,
    isLoading: summaryLoading,
    error: summaryError,
  } = useSummary();
  const newDraftMutation = useNewDraftId();

  const handleNewFlight = () =>
    newDraftMutation.mutate(undefined, {
      onSuccess: (response) => {
        toDraft(response.data._id);
      },
      onError: () => {
        error("Error occurred");
      },
    });

  const handleFinishEditingDraft = () => {
    if (!summaryData?.data.latestDraftId) return;
    toDraft(summaryData.data.latestDraftId);
  };

  const handleToLatestFlight = () => {
    if (!summaryData?.data.latestFlightId) return;
    toFlight(summaryData.data.latestFlightId);
  };

  return (
    <HomeWrapper>
      {summaryLoading ? null : (
        <>
          <HeaderWrapper>
            <H3>Welcome to Offblocks!</H3>
            <H5>Your personal flight data tracker</H5>
          </HeaderWrapper>

          <Divider />

          <VerticalWrapper>
            <H6>Your stats</H6>
            <StatsWrapper>
              <StatCard
                text="Hrs this month"
                value={
                  summaryData?.data.currentMonthMinutes
                    ? minutesToHours(
                        summaryData?.data.currentMonthMinutes,
                      ).toString()
                    : "-"
                }
              />
              <StatCard
                text="Hrs in total"
                value={
                  summaryData?.data.currentMonthMinutes
                    ? minutesToHours(summaryData?.data.totalMinutes).toString()
                    : "-"
                }
              />
              <StatCard
                text="Flights total"
                value={summaryData?.data.flightsNumber.toString() || "?"}
              />
              <StatCard
                text="Aircrafts"
                value={summaryData?.data.aircraftsNumber.toString() || "?"}
              />
            </StatsWrapper>
          </VerticalWrapper>

          <Divider />

          <VerticalWrapper>
            <H6>Quick actions</H6>
            <ShortcutsContainer>
              <ShortcutButton
                variant="contained"
                color="success"
                onClick={handleNewFlight}
                disabled={!!summaryError}
              >
                Add flight
              </ShortcutButton>
              <ShortcutButton
                variant="outlined"
                disabled={!!summaryError || !summaryData?.data.latestFlightId}
                onClick={handleToLatestFlight}
              >
                View latest flight
              </ShortcutButton>
              <ShortcutButton
                variant="contained"
                color="success"
                disabled={!!summaryError || !summaryData?.data.latestDraftId}
                onClick={handleFinishEditingDraft}
              >
                Finish editing draft
              </ShortcutButton>
              <ShortcutButton
                variant="outlined"
                disabled={!!summaryError}
                onClick={toDrafts}
              >
                View drafts
              </ShortcutButton>
              <ShortcutButton
                variant="outlined"
                disabled={!!summaryError}
                onClick={toFlights}
              >
                View flights
              </ShortcutButton>
              <ShortcutButton
                variant="outlined"
                disabled={!!summaryError}
                onClick={toAircrafts}
              >
                View aircrafts
              </ShortcutButton>
            </ShortcutsContainer>
          </VerticalWrapper>
        </>
      )}
    </HomeWrapper>
  );
};

export default Home;
