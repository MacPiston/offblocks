import { Box, styled } from "@mui/material";

export const AircraftsOutletWrapper = styled(Box)`
  height: 100%;
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
`;
