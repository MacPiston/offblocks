import { styled } from "@mui/material";
import { Box } from "@mui/system";

export const FormWrapper = styled(Box)`
  display: flex;
  flex-direction: column;

  width: 100%;

  justify-content: center;
  align-items: center;

  ${({ theme }) => theme.breakpoints.up("sm")} {
    width: 80%;
    flex-direction: row;

    & > *:not(:first-child) {
      margin-left: 1rem;
    }
  }
`;
