import React from "react";
import { Field, FormikProps } from "formik";

import { NewAircraftModel } from "@src/modules/Aircrafts/hooks/useNewAircraft";
import { FormWrapper } from "@src/modules/Aircrafts/Components/NewAircraft/NewAircraftFormContent/NewAircraftFormContent.components";
import TextField from "@src/modules/Shared/components/InputFields";

type Props = FormikProps<NewAircraftModel>;

const NewAircraftFormContent: React.FunctionComponent<Props> = () => (
  <FormWrapper>
    <Field name="name" title="Aircraft name (optional)" component={TextField} />
    <Field name="registration" title="Registration" component={TextField} />
    <Field name="type" title="Type" component={TextField} />
  </FormWrapper>
);

export default NewAircraftFormContent;
