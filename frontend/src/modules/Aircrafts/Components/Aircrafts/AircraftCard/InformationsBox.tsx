import React from "react";

import {
  InfoBox,
  InfoRow,
} from "@src/modules/Aircrafts/Components/Aircrafts/AircraftCard/AircraftCard.components";
import {
  Sub1Bold,
  Sub1Standard,
} from "@src/modules/Shared/components/Typography";

interface Props {
  id: string;
  type: string;
  hours: number;
  lastFlight: Date;
}

const InformationsBox: React.FunctionComponent<Props> = ({
  // id,
  type,
  hours,
  lastFlight,
}) => (
  <InfoBox>
    <InfoRow>
      <Sub1Bold>Type: </Sub1Bold>
      <Sub1Standard>{type}</Sub1Standard>
    </InfoRow>
    <InfoRow>
      <Sub1Bold>Hours flown: </Sub1Bold>
      <Sub1Standard>{hours}</Sub1Standard>
    </InfoRow>
    <InfoRow>
      <Sub1Bold>Last flight on: </Sub1Bold>
      <Sub1Standard>{lastFlight.toLocaleDateString()}</Sub1Standard>
    </InfoRow>
  </InfoBox>
);

export default InformationsBox;
