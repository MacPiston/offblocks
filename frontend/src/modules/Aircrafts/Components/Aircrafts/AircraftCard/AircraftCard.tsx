import React, { useState } from "react";
import { Button, Grid, Menu, MenuItem } from "@mui/material";

import useDeleteAircraft from "@src/modules/Aircrafts/hooks/useDeleteAircraft";
import useNotifications from "@src/modules/Shared/hooks/useNotifications";

import {
  StyledCard,
  StyledCardContent,
} from "@src/modules/Aircrafts/Components/Aircrafts/AircraftCard/AircraftCard.components";
import { H6 } from "@src/modules/Shared/components/Typography";
import DeletionConfirmation from "@src/modules/Aircrafts/Components/Aircrafts/AircraftCard/DeletionConfirmation";
import InformationsBox from "@src/modules/Aircrafts/Components/Aircrafts/AircraftCard/InformationsBox";

import MoreVertIcon from "@mui/icons-material/MoreVert";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

interface Props {
  id: string;
  type: string;
  registration: string;
  hours: number;
  lastFlight: Date;
  onReload: () => void;
}

const AircraftCard: React.FunctionComponent<Props> = ({
  id,
  type,
  registration,
  hours,
  lastFlight,
  onReload,
}) => {
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [confirmationOpen, setConfirmationOpen] = useState<boolean>(false);
  const menuOpen = !!anchorEl;

  const { success, error } = useNotifications();
  const { mutate: deleteAircraft } = useDeleteAircraft(id);

  const onMoreClicked = (event: React.MouseEvent<HTMLButtonElement>) =>
    setAnchorEl(event.currentTarget);

  const onHideMenu = () => setAnchorEl(null);

  const onDelete = async () =>
    deleteAircraft(undefined, {
      onSuccess: () => {
        success("Aircraft deleted!");
        onReload();
      },
      onError: (err) => {
        error(err.response?.data.message);
      },
    });

  return (
    <StyledCard>
      <StyledCardContent>
        <Grid container spacing={1}>
          <Grid item xs={11} sm={10}>
            <H6>{registration}</H6>
          </Grid>
          <Grid item xs={1} sm={2}>
            <Button
              onClick={onMoreClicked}
              style={{ padding: "0", minWidth: "unset" }}
            >
              <MoreVertIcon />
            </Button>
            <Menu anchorEl={anchorEl} open={menuOpen} onClose={onHideMenu}>
              <MenuItem onClick={() => setConfirmationOpen(true)}>
                <DeleteForeverIcon />
                Delete
              </MenuItem>
            </Menu>
            <DeletionConfirmation
              isOpen={confirmationOpen}
              onClose={() => {
                setConfirmationOpen(false);
                setAnchorEl(null);
              }}
              onDelete={onDelete}
            />
          </Grid>
        </Grid>
        <InformationsBox
          id={id}
          type={type}
          hours={hours}
          lastFlight={lastFlight}
        />
      </StyledCardContent>
    </StyledCard>
  );
};

export default AircraftCard;
