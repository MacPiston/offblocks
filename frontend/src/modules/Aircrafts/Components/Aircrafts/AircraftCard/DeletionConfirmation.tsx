import React from "react";

import { Button, Dialog, DialogTitle } from "@mui/material";

interface Props {
  isOpen: boolean;
  onClose: () => void;
  onDelete: () => void;
}

const DeletionConfirmation: React.FunctionComponent<Props> = ({
  isOpen,
  onClose,
  onDelete,
}) => {
  return (
    <Dialog open={isOpen} onClose={onClose}>
      <DialogTitle>Do you really want to delete this aircraft?</DialogTitle>
      <div style={{ margin: "0 auto 8px auto" }}>
        <Button style={{ marginRight: "8px" }} onClick={onClose}>
          Cancel
        </Button>
        <Button color="warning" variant="contained" onClick={onDelete}>
          Delete
        </Button>
      </div>
    </Dialog>
  );
};

export default DeletionConfirmation;
