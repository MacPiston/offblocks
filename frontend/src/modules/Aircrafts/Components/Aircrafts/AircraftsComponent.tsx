import React from "react";
import { Button, Grid } from "@mui/material";

import useAircrafts from "@src/modules/Aircrafts/hooks/useAircrafts";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";

import AircraftCard from "@src/modules/Aircrafts/Components/Aircrafts/AircraftCard";
import {
  AircraftsWrapper,
  StyledGrid,
} from "@src/modules/Aircrafts/Components/Aircrafts/AircraftsComponent.components";
import HeaderWithButtons from "@src/modules/Shared/components/HeaderWithButtons";
import Spinner from "@src/modules/Shared/components/Spinner";

const AircraftsComponents = () => {
  const { toNewAircraft } = useRedirects();
  const { isLoading, data, refetch } = useAircrafts();

  return (
    <AircraftsWrapper>
      <HeaderWithButtons
        title="Aircrafts"
        buttons={
          <Button variant="contained" onClick={toNewAircraft}>
            Add
          </Button>
        }
      />
      <StyledGrid container spacing={2}>
        {isLoading ? (
          <Grid item md={12}>
            <Spinner />
          </Grid>
        ) : (
          data?.data.map((a) => (
            <Grid key={a._id} item xs={12} sm={3}>
              <AircraftCard
                id={a._id}
                type={a.type}
                registration={a.registration}
                hours={0}
                lastFlight={new Date()}
                onReload={refetch}
              />
            </Grid>
          ))
        )}
      </StyledGrid>
    </AircraftsWrapper>
  );
};

export default AircraftsComponents;
