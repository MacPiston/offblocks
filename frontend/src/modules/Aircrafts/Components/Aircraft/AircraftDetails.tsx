import React from "react";

import { ListItem } from "@mui/material";
import { DetailsList } from "@src/modules/Aircrafts/Components/Aircraft/AircraftComponent.components";
import {
  Sub1Bold,
  Sub1Standard,
} from "@src/modules/Shared/components/Typography";

interface Props {
  type: string;
  registration: string;
  flights: number;
  lastFlight: Date;
}

const AircraftDetails: React.FunctionComponent<Props> = ({
  type,
  registration,
  flights,
  lastFlight,
}) => (
  <DetailsList>
    <ListItem>
      <Sub1Bold>Type:&nbsp;</Sub1Bold>
      <Sub1Standard>{type}</Sub1Standard>
    </ListItem>
    <ListItem>
      <Sub1Bold>Registration:&nbsp;</Sub1Bold>
      <Sub1Standard>{registration}</Sub1Standard>
    </ListItem>
    <ListItem>
      <Sub1Bold>Flights:&nbsp;</Sub1Bold>
      <Sub1Standard>{flights}</Sub1Standard>
    </ListItem>
    <ListItem>
      <Sub1Bold>Last flight:&nbsp;</Sub1Bold>
      <Sub1Standard>{lastFlight.toLocaleDateString()}</Sub1Standard>
    </ListItem>
  </DetailsList>
);

export default AircraftDetails;
