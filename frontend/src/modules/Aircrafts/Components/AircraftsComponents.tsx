import React from "react";
import { Outlet } from "react-router-dom";

import { AircraftsOutletWrapper } from "@src/modules/Aircrafts/Aircrafts.components";

const AircraftsComponents = () => (
  <AircraftsOutletWrapper>
    <Outlet />
  </AircraftsOutletWrapper>
);

export default AircraftsComponents;
