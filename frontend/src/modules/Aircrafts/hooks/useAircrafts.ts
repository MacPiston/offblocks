import { useQuery } from "react-query";
import { AxiosError, AxiosResponse } from "axios";

import useAxios from "@src/common/axios/useAxios";
import { AIRCRAFT_ENDPOINT } from "@src/config/baseUrls";
import { AircraftModel } from "@src/modules/Aircrafts/hooks/useAircraft";

const useAircrafts = () => {
  const axios = useAxios();

  return useQuery<AxiosResponse<AircraftModel[]>, AxiosError>(
    "fetch-aircrafts",
    async () => await axios.get(`${AIRCRAFT_ENDPOINT}/all`),
  );
};

export default useAircrafts;
