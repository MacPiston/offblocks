import { useMutation } from "react-query";
import { AxiosResponse, AxiosError } from "axios";

import useAxios from "@src/common/axios/useAxios";
import { AIRCRAFT_ENDPOINT } from "@src/config/baseUrls";

const useDeleteAircraft = (aircraftId: string) => {
  const axios = useAxios();

  return useMutation<AxiosResponse, AxiosError>(
    async () => await axios.delete(`${AIRCRAFT_ENDPOINT}/${aircraftId}`),
  );
};

export default useDeleteAircraft;
