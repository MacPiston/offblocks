import React, { FunctionComponent } from "react";

import { LandingWrapper } from "@src/modules/Landing/Landing.components";
import LoginForm from "@src/modules/Shared/LoginForm/LoginForm";

const Landing: FunctionComponent = () => (
  <LandingWrapper>
    <LoginForm title="Login" />
  </LandingWrapper>
);

export default Landing;
