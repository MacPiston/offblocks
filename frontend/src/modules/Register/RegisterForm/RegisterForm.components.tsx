import { Button, Paper, styled, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { Form } from "formik";

export const RegisterFormBackground = styled(Paper)`
  ${({ theme }) => theme.breakpoints.down("sm")} {
    width: 100%;
  }
  width: 520px;
  padding: ${({ theme }) => theme.spacing(2)};
`;

export const StyledForm = styled(Form)`
  display: flex;
  flex-direction: column;

  justify-content: center;
  align-items: center;
`;

export const RegisterFormHeader = styled(Typography)`
  margin-bottom: ${({ theme }) => theme.spacing(1)};
`;

export const TextFieldsWrapper = styled(Box)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  width: 100%;

  & > * {
    :not(:first-child) {
      margin-top: ${({ theme }) => theme.spacing(1)};
    }
  }
`;

export const HorizontalFields = styled(Box)`
  display: flex;
  justify-content: space-evenly;
  align-items: baseline;

  & > :nth-child(2) {
    margin-left: ${({ theme }) => theme.spacing(2)};
  }
`;

export const StyledButton = styled(Button)`
  margin-top: ${({ theme }) => theme.spacing(2)};
`;

export const SuccessWrapper = styled(Box)`
  display: flex;
  flex-direction: column;
  align-items: center;

  & > :nth-child(2) {
    margin-top: ${({ theme }) => theme.spacing(1)};
  }
`;
