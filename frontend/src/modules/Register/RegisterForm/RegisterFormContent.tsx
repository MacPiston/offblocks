import { Field, FormikProps } from "formik";
import React from "react";
import { Box, CircularProgress } from "@mui/material";

import useRedirects from "@src/modules/Shared/hooks/useRedirects";

import TextField from "@src/modules/Shared/components/InputFields";
import { RegisterModel } from "@src/modules/Register/hooks/useRegister";
import {
  HorizontalFields,
  RegisterFormHeader,
  StyledButton,
  StyledForm,
  TextFieldsWrapper,
} from "@src/modules/Register/RegisterForm/RegisterForm.components";

export type RegisterFormModel = RegisterModel & { password2: string };

type Props = FormikProps<RegisterFormModel>;

const RegisterFormContent: React.FunctionComponent<Props> = ({
  isSubmitting,
}) => {
  const { toLanding } = useRedirects();

  return (
    <StyledForm>
      <RegisterFormHeader variant="h4">Register new user</RegisterFormHeader>
      <TextFieldsWrapper>
        <Field
          name="email"
          title="Your email"
          component={TextField}
          placeholder="Valid email address"
        />
        <Field
          name="username"
          title="Your username"
          component={TextField}
          placeholder="Other users will see you under that name"
        />
        <HorizontalFields>
          <Field
            name="password"
            title="password"
            type="password"
            component={TextField}
            placeholder="Enter password"
          />
          <Field
            name="password2"
            title="repeat password"
            type="password"
            component={TextField}
            placeholder="Repeat password"
          />
        </HorizontalFields>
      </TextFieldsWrapper>
      <Box sx={{ display: "flex" }}>
        <StyledButton
          type="submit"
          variant="contained"
          size="large"
          sx={{ marginRight: 2 }}
        >
          {isSubmitting ? (
            <CircularProgress
              sx={{ maxHeight: 24, maxWidth: 24 }}
              color="secondary"
            />
          ) : (
            "Register"
          )}
        </StyledButton>
        <StyledButton
          onClick={toLanding}
          type="button"
          variant="outlined"
          size="large"
        >
          Login
        </StyledButton>
      </Box>
    </StyledForm>
  );
};

export default RegisterFormContent;
