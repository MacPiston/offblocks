import styled from "styled-components";

import { Box } from "@mui/material";

export const RegisterWrapper = styled(Box)`
  width: 100%;
  height: 100%;

  display: flex;
  justify-content: center;
  align-items: center;
`;
