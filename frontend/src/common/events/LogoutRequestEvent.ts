import { appEventTypes } from "@src/common/events/AppEventTarget";

interface LogoutRequestEventDetail {
  showToast: boolean;
  redirectPathname?: string;
}

class LogoutRequestEvent extends CustomEvent<LogoutRequestEventDetail> {
  constructor(settings?: { showToast?: boolean; redirectPathname?: string }) {
    super(appEventTypes.LOGOUT_REQUEST, {
      detail: {
        showToast: settings?.showToast ?? false,
        redirectPathname: settings?.redirectPathname,
      },
    });
  }
}

export default LogoutRequestEvent;
