import { createTheme } from "@mui/material";
import createPalette, {
  PaletteOptions,
} from "@mui/material/styles/createPalette";

const paletteOverride: PaletteOptions = {
  primary: {
    main: "#039be5",
    light: "#35AFEA",
    dark: "#026CA0",
    contrastText: "#FFF",
  },
  secondary: {
    main: "#4fc3f7",
    light: "#72CFF8",
    dark: "#3788AC",
    contrastText: "rgba(0, 0, 0, 0.87)",
  },
  error: {
    main: "#f44336",
    light: "#e57373",
    dark: "#d32f2f",
    contrastText: "#fff",
  },
  warning: {
    main: "#ff9800",
    light: "#ffb74d",
    dark: "#f57c00",
    contrastText: "rgba(0, 0, 0, 0.87)",
  },
  info: {
    main: "#2196f3",
    light: "#64b5f6",
    dark: "#1976d2",
    contrastText: "#fff",
  },
  success: {
    main: "#4caf50",
    light: "#81c784",
    dark: "#388e3c",
    contrastText: "rgba(0, 0, 0, 0.87)",
  },
  divider: "rgba(0, 0, 0, 0.12)",
};

const customPalette = createPalette(paletteOverride);

export const customTheme = createTheme({
  palette: customPalette,
});
