import axios, { AxiosRequestConfig } from "axios";
import React, { useMemo } from "react";
import { differenceInMinutes } from "date-fns";

import AppEventTarget from "@src/common/events/AppEventTarget";
import {
  deleteUserToken,
  getTimeToExpire,
  getUserToken,
} from "@src/common/helpers/userToken";
import { AxiosContext } from "@src/common/axios/AxiosContext";
import { API_URL } from "@src/config/baseUrls";
import LogoutRequestEvent from "@src/common/events/LogoutRequestEvent";
import useNotifications from "@src/modules/Shared/hooks/useNotifications";
import useRedirects from "@src/modules/Shared/hooks/useRedirects";
import useRefreshToken from "@src/common/axios/useRefreshToken";

export const baseAxiosHeaders = {
  "Content-Type": "application/json",
};

export const baseAxiosConfig: AxiosRequestConfig = {
  withCredentials: true,
  baseURL: API_URL,
};

const AxiosProvider = ({ children }: React.PropsWithChildren<unknown>) => {
  const refreshToken = useRefreshToken();
  const { error: errorNotification } = useNotifications();
  const { toLanding } = useRedirects();

  const axiosValue = useMemo(() => {
    const axiosInstance = axios.create({
      headers: {
        ...baseAxiosHeaders,
      },
      ...baseAxiosConfig,
    });

    axiosInstance.interceptors.request.use(async (config) => {
      const userToken = getUserToken();
      if (userToken !== null) {
        const validDuration = differenceInMinutes(
          userToken.expiresAt,
          userToken.issuedAt,
        );
        const diff = getTimeToExpire(userToken);
        if (diff < validDuration / 2) {
          try {
            await refreshToken(userToken);
          } catch (error) {
            console.error(error);
            console.error("Unable to refresh token", error);
            AppEventTarget.dispatchEvent(new LogoutRequestEvent());
          }
        }
      }

      if (userToken) {
        config.headers!.Authorization = `Bearer ${userToken.token}`;
      }

      return config;
    });

    axiosInstance.interceptors.response.use(undefined, async (error) => {
      let errorMsg;
      switch (error.response.status) {
        case 401:
          errorMsg = error.response.data.message;
          if (errorMsg === "Email not verified")
            errorNotification("Email not verified; activation link resent");
          else errorNotification("Please log in");
          deleteUserToken();
          toLanding();
          break;
        default:
          errorMsg =
            error.response.data.message ??
            (Object.values(error.response?.data.errors)[0] as string[])[0] ??
            "Something went wrong, please try again.";
          break;
      }

      return Promise.reject(error);
    });

    return axiosInstance;
  }, [errorNotification, toLanding, refreshToken]);

  return (
    <AxiosContext.Provider value={axiosValue}>{children}</AxiosContext.Provider>
  );
};

export default AxiosProvider;
