import { useContext } from "react";

import { AxiosContext } from "@src/common/axios/AxiosContext";

const useAxios = () => useContext(AxiosContext);

export default useAxios;
