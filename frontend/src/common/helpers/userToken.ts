import jwt_decode, { JwtPayload } from "jwt-decode";
import { differenceInMinutes } from "date-fns";

import AppEventTarget from "@src/common/events/AppEventTarget";
import TokenChangeEvent from "@src/common/events/TokenChangeEvent";

const USER_TOKEN_KEY = "usertoken";

export interface UserToken {
  token: string;
  issuedAt: Date;
  expiresAt: Date;
  email: string;
  username: string;
}

export interface LoginResponse {
  token: string;
  email: string;
  username: string;
}

export interface RegisterResponse {
  emailVerified: boolean;
  email: string;
  username: string;
}

export const userTokenFromResponse = (
  response: LoginResponse,
): UserToken | null => {
  const decodedToken = jwt_decode<JwtPayload>(response.token);
  try {
    if (decodedToken.iat === undefined || decodedToken.exp === undefined) {
      throw new Error("Invalid decoded token dates");
    }
  } catch (error) {
    return null;
  }

  if (!response.email || !response.username) return null;

  return {
    token: response.token,
    issuedAt: new Date(decodedToken.iat * 1000),
    expiresAt: new Date(decodedToken.exp * 1000),
    email: response.email,
    username: response.username,
  };
};

export const setUserToken = (token: UserToken | null) => {
  localStorage.setItem(USER_TOKEN_KEY, JSON.stringify(token));
  AppEventTarget.dispatchEvent(new TokenChangeEvent(token));
};

export const getUserToken = (): UserToken | null => {
  try {
    const userToken = JSON.parse(
      localStorage.getItem(USER_TOKEN_KEY) ?? "null",
    );

    if (userToken === null) {
      return null;
    }

    ["token", "issuedAt", "expiresAt"].forEach((key) => {
      if (!(key in userToken)) {
        throw new Error(`Missing key: ${key}`);
      }
    });

    userToken.issuedAt = new Date(userToken.issuedAt);
    if (isNaN(userToken.issuedAt)) {
      throw new Error("Invalid issue date");
    }

    userToken.expiresAt = new Date(userToken.expiresAt);
    if (isNaN(userToken.expiresAt)) {
      throw new Error("Invalid expiration date");
    }

    return userToken;
  } catch (err) {
    console.error(`Error parsing user token ${(err as any).message}`);
    deleteUserToken();
    return null;
  }
};

export const getTimeToExpire = (token: UserToken) =>
  differenceInMinutes(token.expiresAt, new Date());

export const deleteUserToken = () => setUserToken(null);
