import { format, minutesToHours, parseISO } from "date-fns";

export const dateToDateTimeString = (date?: Date) => {
  if (!date) return "";
  return `${format(date, "yyyy-MM-dd")}T${format(date, "HH:mm")}`;
};

export const timeStringToMinutes = (timeString?: string) => {
  if (!timeString) return undefined;
  const matches = /(\d{2}):(\d{2})/g.exec(timeString);
  if (!matches) return undefined;

  const hours = parseInt(matches[1]);
  const minutes = parseInt(matches[2]);

  return hours * 60 + minutes;
};

export const minutesToTimeString = (minutes?: number) => {
  if (!minutes) return "";

  const parseTwoDigits = (value: number) => {
    if (value < 10) return `0${value}`;
    return `${value}`;
  };

  const hours = minutesToHours(minutes);
  if (hours === 0) return `00:${parseTwoDigits(minutes)}`;
  return `${parseTwoDigits(hours)}:${parseTwoDigits(minutes - hours * 60)}`;
};

export const dateFromISOString = (dateString?: string) => {
  if (!dateString) return new Date();
  return parseISO(dateString);
};
