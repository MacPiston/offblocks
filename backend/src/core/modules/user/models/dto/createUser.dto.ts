import { IsEmail, IsNotEmpty, MaxLength, MinLength } from "class-validator";

export class CreateUserDto {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(15)
  username: string;

  @IsNotEmpty()
  @MinLength(8)
  password: string;
}
