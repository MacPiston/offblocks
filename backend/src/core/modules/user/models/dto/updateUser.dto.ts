import { IsOptional, MaxLength, MinLength } from "class-validator";

export class UpdateUserDto {
  @IsOptional()
  licenceNumber?: string;

  @IsOptional()
  @MinLength(5)
  @MaxLength(15)
  username?: string;
}
