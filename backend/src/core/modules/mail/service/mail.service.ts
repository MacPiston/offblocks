import { ISendMailOptions, MailerService } from "@nestjs-modules/mailer";
import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { I18nRequestScopeService } from "nestjs-i18n";

import { UserDocument } from "../../../schemas/user/user.schema";

@Injectable()
export class MailService {
  private readonly from: string;

  constructor(
    private readonly mailerService: MailerService,
    private readonly configService: ConfigService,
    private readonly i18n: I18nRequestScopeService
  ) {
    this.from = configService.get<string>("SMTP.FROM");
  }

  async sendActivationEmail(user: UserDocument, token: string, lang: string) {
    const subject = await this.i18n.translate("templates.activation", { lang });
    const activationLink = `${this.configService.get<string>(
      "FRONTURL"
    )}/activate/${token}`;

    const options: ISendMailOptions = {
      to: user.email,
      from: this.from,
      subject,
      template: `./confirmEmail_${lang}.hbs`,
      context: {
        username: user.username,
        link: activationLink,
      },
    };

    await this.mailerService.sendMail(options);
  }
}
