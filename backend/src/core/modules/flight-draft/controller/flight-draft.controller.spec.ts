import { Test, TestingModule } from "@nestjs/testing";
import { FlightDraftController } from "./flight-draft.controller";

describe("FlightDraftController", () => {
  let controller: FlightDraftController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FlightDraftController],
    }).compile();

    controller = module.get<FlightDraftController>(FlightDraftController);
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
});
