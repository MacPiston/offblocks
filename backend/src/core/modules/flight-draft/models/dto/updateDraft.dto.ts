import {
  IsDateString,
  IsInt,
  IsOptional,
  IsString,
  Min,
  MinLength,
  ValidateNested,
} from "class-validator";

import { Landings } from "../../../../schemas/flight/subschemas/landings";
import { OperationalConditionTime } from "../../../../schemas/flight/subschemas/operationalConditionTime";
import { PathPoint } from "../../../../schemas/flight/subschemas/pathpoint";
import { PilotFunctionTime } from "../../../../schemas/flight/subschemas/pilotFunctionTime";
import { PilotTime } from "../../../../schemas/flight/subschemas/pilotTime";
import { SyntheticTrainingDeviceSession } from "../../../../schemas/flight/subschemas/synteticTrainingDeviceSession";

export class UpdateDraftDto {
  @IsOptional()
  @IsDateString()
  departureDate?: string;

  @IsOptional()
  @ValidateNested()
  departurePoint?: PathPoint;

  @IsOptional()
  @MinLength(1, { each: true })
  @ValidateNested()
  pathPoints?: PathPoint[];

  @IsOptional()
  @IsDateString()
  arrivalDate?: string;

  @IsOptional()
  @ValidateNested()
  arrivalPoint?: PathPoint;

  @IsOptional()
  aircraftId?: string;

  @IsOptional()
  @ValidateNested()
  pilotTime?: PilotTime;

  @IsOptional()
  @IsString()
  pic?: string;

  @IsOptional()
  @ValidateNested()
  landings?: Landings;

  @IsOptional()
  @IsInt()
  @Min(0)
  ifrApproaches?: number;

  @IsOptional()
  @ValidateNested()
  oct?: OperationalConditionTime;

  @IsOptional()
  @ValidateNested()
  pft?: PilotFunctionTime;

  @IsOptional()
  @ValidateNested()
  stds?: SyntheticTrainingDeviceSession;

  @IsOptional()
  @IsString()
  remarks?: string;
}
