import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { ConfigModule } from "@nestjs/config";

import { FlightDraftService } from "./service/flight-draft.service";
import { FlightDraftController } from "./controller/flight-draft.controller";
import {
  FlightDraft,
  FlightDraftSchema,
} from "../../schemas/flight-draft/flightDraft.schema";
import { UserModule } from "../user/user.module";
import { JwtStrategy } from "../../strategies/jwt.strategy";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: FlightDraft.name, schema: FlightDraftSchema },
    ]),
    UserModule,
    ConfigModule,
  ],
  controllers: [FlightDraftController],
  providers: [FlightDraftService, JwtStrategy],
  exports: [FlightDraftService],
})
export class FlightDraftModule {}
