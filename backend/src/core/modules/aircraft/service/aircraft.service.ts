import {
  ConflictException,
  Injectable,
  NotFoundException,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";

import {
  Aircraft,
  AircraftDocument,
} from "../../../schemas/aircraft/aircraft.schema";
import { User } from "../../../schemas/user/user.schema";
import { UserService } from "../../user/service/user.service";
import { CreateAircraftDto } from "../models/dto/createAircraft.dto";
import { UpdateAircraftDto } from "../models/dto/updateAircraft.dto";

@Injectable()
export class AircraftService {
  constructor(
    @InjectModel(Aircraft.name)
    private readonly aircraftModel: Model<AircraftDocument>,
    private readonly userService: UserService
  ) {}

  async findAll(userId: string): Promise<Aircraft[] | undefined> {
    const currentUser = await this.userService.getById(userId);

    return await this.aircraftModel.find({ owner: currentUser });
  }

  async findById(userId: string, aircraftId: string) {
    const currentUser = await this.userService.getById(userId);
    const currentAircraft = await this.aircraftModel.findOne({
      _id: aircraftId,
      owner: currentUser,
    });
    if (!currentAircraft) throw new NotFoundException("Aircraft not found");

    return currentAircraft;
  }

  async createAircraft(userId: string, dto: CreateAircraftDto) {
    const currentUser = await this.userService.getById(userId);

    await this.checkForDuplicate(dto, currentUser);

    return await this.aircraftModel.create({ ...dto, owner: currentUser });
  }

  async updateAircraft(aircraftId: string, dto: UpdateAircraftDto) {
    const updatedAircraft = await this.aircraftModel.findByIdAndUpdate(
      aircraftId,
      {
        $set: { ...dto },
      },
      {
        new: true,
      }
    );
    if (!updatedAircraft) throw new NotFoundException("Aircraft not found");

    return updatedAircraft;
  }

  async deleteAircraft(userId: string, aircraftId: string) {
    const currentUser = await this.userService.getById(userId);

    return await this.aircraftModel.deleteOne({
      _id: aircraftId,
      owner: currentUser,
    });
  }

  // PRIVATE HELPER METHODS

  private async checkForDuplicate(dto: CreateAircraftDto, owner: User) {
    const { registration, name, type } = dto;
    if (
      await this.aircraftModel.findOne({
        registration,
        name,
        type,
        owner,
      })
    )
      throw new ConflictException("Aircraft already exists!");
  }
}
