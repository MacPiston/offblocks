import { IsOptional, IsString, Length, MaxLength } from "class-validator";

export class UpdateAircraftDto {
  @IsOptional()
  @IsString()
  @MaxLength(10)
  registration?: string;

  @IsOptional()
  @IsString()
  @MaxLength(15)
  name?: string;

  @IsOptional()
  @IsString()
  @MaxLength(10)
  type?: string;
}
