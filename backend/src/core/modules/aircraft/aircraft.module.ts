import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { MongooseModule } from "@nestjs/mongoose";

import {
  Aircraft,
  AircraftSchema,
} from "../../schemas/aircraft/aircraft.schema";
import { JwtStrategy } from "../../strategies/jwt.strategy";
import { UserModule } from "../user/user.module";
import { AircraftController } from "./controller/aircraft.controller";
import { AircraftService } from "./service/aircraft.service";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Aircraft.name, schema: AircraftSchema },
    ]),
    UserModule,
    ConfigModule,
  ],
  controllers: [AircraftController],
  providers: [AircraftService, JwtStrategy],
  exports: [AircraftService],
})
export class AircraftModule {}
