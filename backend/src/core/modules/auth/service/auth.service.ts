import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { JwtService } from "@nestjs/jwt";
import { Model } from "mongoose";
import * as bcrypt from "bcrypt";
import { ConfigService } from "@nestjs/config";

import { User, UserDocument } from "../../../schemas/user/user.schema";
import { JwtPayload } from "../models/jwtPayload.model";
import { MailService } from "src/core/modules/mail/service/mail.service";

@Injectable()
export class AuthService {
  private readonly verificationSecret: string;
  private readonly verificationExpiration = "24h";
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
    private readonly mailService: MailService
  ) {
    this.verificationSecret =
      this.configService.get<string>("VERIFICATION_JWT");
  }

  async login(email: string, pwd: string) {
    const user = await this.userModel.findOne({ email: email });

    if (!user) throw new NotFoundException();
    if (!user.emailVerified) {
      const newToken = await this.getEmailVerificationToken(user);
      await this.mailService.sendActivationEmail(user, newToken, "en");
      throw new UnauthorizedException("Email not verified");
    }
    if (!(await this.validatePassword(user, pwd)))
      throw new UnauthorizedException();

    const token = this.getToken(user);

    return { token, email: user.email, username: user.username };
  }

  async refreshToken(userId: string) {
    const currentUser = await this.userModel.findById(userId);
    if (!currentUser) throw new NotFoundException();

    const token = this.getToken(currentUser);
    return { token, email: currentUser.email, username: currentUser.username };
  }

  async getEmailVerificationToken(user: UserDocument): Promise<string> {
    const payload: JwtPayload = { username: user.username, _id: user._id };
    const token = this.jwtService.sign(payload, {
      secret: this.verificationSecret,
      expiresIn: this.verificationExpiration,
    });

    return token;
  }

  async verifyEmail(token: string) {
    const { _id } = await this.validateConfirmationToken(token);
    const user = await this.userModel.findById(_id);

    if (user.emailVerified)
      throw new BadRequestException("Email already verified");

    return await this.userModel.findOneAndUpdate(
      { id: _id },
      { emailVerified: true }
    );
  }

  // PRIVATE HELPER METHODS

  private async validatePassword(
    user: UserDocument,
    password: string
  ): Promise<boolean> {
    return bcrypt.compare(password, user.password);
  }

  private getToken(user: UserDocument): string {
    const payload: JwtPayload = { username: user.username, _id: user._id };

    return this.jwtService.sign(payload);
  }

  private async validateConfirmationToken(token: string): Promise<JwtPayload> {
    try {
      const payload = await this.jwtService.verify(token, {
        secret: this.verificationSecret,
      });
      if (
        typeof payload === "object" &&
        "username" in payload &&
        "_id" in payload
      )
        return payload;
      throw new BadRequestException("Bad payload");
    } catch (error) {
      if (error?.name === "TokenExpiredError")
        throw new BadRequestException("Confirmation token expired");
      throw new BadRequestException("Bad confirmation token");
    }
  }
}
