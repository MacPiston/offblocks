import { IsJWT, IsNotEmpty } from "class-validator";

export class ConfirmDto {
  @IsNotEmpty()
  @IsJWT()
  token: string;
}
