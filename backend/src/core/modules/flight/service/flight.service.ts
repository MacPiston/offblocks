import {
  BadRequestException,
  ConflictException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { isBefore } from "date-fns";
import { Model } from "mongoose";
import { AircraftService } from "src/core/modules/aircraft/service/aircraft.service";
import { UpdateFlightDto } from "src/core/modules/flight/models/dto/updateFlight.dto";
import {
  Aircraft,
  AircraftDocument,
} from "src/core/schemas/aircraft/aircraft.schema";
import { Landings } from "src/core/schemas/flight/subschemas/landings";
import { OperationalConditionTime } from "src/core/schemas/flight/subschemas/operationalConditionTime";
import {
  PathPoint,
  PointType,
} from "src/core/schemas/flight/subschemas/pathpoint";
import { PilotFunctionTime } from "src/core/schemas/flight/subschemas/pilotFunctionTime";
import { PilotTime } from "src/core/schemas/flight/subschemas/pilotTime";
import { FlightDraft } from "../../../schemas/flight-draft/flightDraft.schema";

import { Flight, FlightDocument } from "../../../schemas/flight/flight.schema";
import { FlightDraftService } from "../../flight-draft/service/flight-draft.service";
import { UserService } from "../../user/service/user.service";

@Injectable()
export class FlightService {
  constructor(
    @InjectModel(Flight.name)
    private readonly flightModel: Model<FlightDocument>,
    @InjectModel(Aircraft.name)
    private readonly aircraftModel: Model<AircraftDocument>,
    private readonly userService: UserService,
    private readonly flightDraftService: FlightDraftService,
    private readonly aircraftService: AircraftService
  ) {}

  async findAll(userId: string): Promise<Flight[] | undefined> {
    const currentUser = await this.userService.getById(userId);
    return await this.flightModel
      .find({ owner: currentUser })
      .populate("aircraft", "", this.aircraftModel);
  }

  async getById(userId: string, flightId: string) {
    const currentUser = await this.userService.getById(userId);
    return await this.flightModel
      .findOne({
        _id: flightId,
        owner: currentUser,
      })
      .populate("aircraft", "", this.aircraftModel);
  }

  async createFlight(userId: string, draftId: string) {
    const currentUser = await this.userService.getById(userId);
    if (!currentUser) throw new NotFoundException("User not found");

    const currentDraft = await this.flightDraftService.getById(draftId);
    if (!currentDraft) throw new NotFoundException("Draft not found");
    if (currentDraft.flightCreated)
      throw new ConflictException("Flight for draft exists");

    const currentAircraft = await this.aircraftService.findById(
      userId,
      currentDraft.aircraftId
    );
    if (!currentAircraft) throw new NotFoundException("Aircraft not found");

    this.validateAircraft(currentAircraft);
    this.validateDraft(currentDraft);

    const {
      departureDate,
      departurePoint,
      arrivalDate,
      arrivalPoint,
      pic,
      landings,
      pilotTime,
      pft,
      oct,
      ifrApproaches,
      stds,
      remarks,
    } = currentDraft;

    const newFlight = await this.flightModel.create({
      owner: currentUser,
      createdFrom: currentDraft,
      createdOn: new Date(),

      departureDate,
      departurePoint,
      arrivalDate,
      arrivalPoint,

      aircraft: currentAircraft,
      pic,
      landings,

      pilotTime,
      pft,
      oct,
      ifrApproaches,
      stds,
      remarks,
    });

    if (!newFlight)
      throw new InternalServerErrorException("Error creating flight");

    await this.flightDraftService.delete(userId, draftId);

    return newFlight;
  }

  async updateFlight(
    draftId: string,
    userId: string,
    dto: UpdateFlightDto
  ): Promise<Flight> {
    const { departureDate, arrivalDate, aircraftId, ...rest } = dto;

    if (aircraftId) {
      const aircraft = await this.aircraftService.findById(userId, aircraftId);

      const updatedFlight = await this.flightModel.findByIdAndUpdate(
        draftId,
        {
          $set: {
            departureDate: new Date(departureDate),
            arrivalDate: new Date(arrivalDate),
            aircraft,
            ...rest,
          },
        },
        {
          new: true,
        }
      );
      if (!updatedFlight) throw new NotFoundException("Draft not found");

      return updatedFlight;
    } else {
      const updatedFlight = await this.flightModel.findByIdAndUpdate(
        draftId,
        {
          $set: {
            departureDate: new Date(departureDate),
            arrivalDate: new Date(arrivalDate),
            ...rest,
          },
        },
        {
          new: true,
        }
      );
      if (!updatedFlight) throw new NotFoundException("Draft not found");

      return updatedFlight;
    }
  }

  async deleteFlight(userId: string, flightId: string) {
    const currentUser = await this.userService.getById(userId);
    await this.flightModel.deleteOne({ _id: flightId, owner: currentUser });
  }

  //PRIVATE HELPER METHODS

  private validateDates(before: Date, after: Date) {
    if (!isBefore(before, after))
      throw new BadRequestException("Departure date after arrival");
  }

  private validateAirportPoint(point: PathPoint) {
    if (Object.keys(point).length === 0)
      throw new BadRequestException("Invalid point");

    const { icaoName, type } = point;
    if (type !== PointType.ARPT)
      throw new BadRequestException("Wrong airport point type");
    if (!icaoName || icaoName.length !== 4)
      throw new BadRequestException("Wrong airport ICAO name");
  }

  private validateAircraft(aircraft: Aircraft) {
    if (Object.keys(aircraft).length === 0)
      throw new BadRequestException("Invalid aircraft provided");

    if (!aircraft) throw new BadRequestException("No aircraft provided");
  }

  private validatePic(pic: string) {
    if (!pic) throw new BadRequestException("No PIC name provided");
  }

  private validateLandings(landings: Landings) {
    if (Object.keys(landings).length === 0)
      throw new BadRequestException("Invalid landings provided");

    const { daytime, nighttime } = landings;
    if (!daytime && !nighttime)
      throw new BadRequestException("Invalid landings provided");
  }

  private validatePilotTime(pilotTime: PilotTime) {
    if (Object.keys(pilotTime).length === 0)
      throw new BadRequestException("Invalid pilotTime provided");

    const { spse, spme, mp } = pilotTime;
    if (!spse && !spme && !mp)
      throw new BadRequestException("Invalid pilotTime provided");
  }

  private validatePft(pft: PilotFunctionTime) {
    if (Object.keys(pft).length === 0)
      throw new BadRequestException("Invalid pft provided");

    const { pic, coPilot, dual, instructor } = pft;
    if (!pic && !coPilot && !dual && !instructor)
      throw new BadRequestException("Invalid pft provided");
  }

  private validateOct(oct: OperationalConditionTime) {
    if (Object.keys(oct).length === 0)
      throw new BadRequestException("Invalid oct provided");
  }

  private validateDraft(draft: FlightDraft) {
    const {
      departureDate,
      departurePoint,
      arrivalDate,
      arrivalPoint,
      pic,
      landings,
      pilotTime,
      pft,
      oct,
    } = draft;

    this.validateDates(departureDate, arrivalDate);
    this.validateAirportPoint(departurePoint);
    this.validateAirportPoint(arrivalPoint);

    this.validatePic(pic);
    this.validateLandings(landings);

    this.validatePilotTime(pilotTime);
    this.validatePft(pft);
    this.validateOct(oct);
  }
}
