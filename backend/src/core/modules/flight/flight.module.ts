import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { ConfigModule } from "@nestjs/config";

import { FlightController } from "./controller/flight.controller";
import { FlightService } from "./service/flight.service";
import { Flight, FlightSchema } from "../../schemas/flight/flight.schema";
import { UserModule } from "../user/user.module";
import { JwtStrategy } from "../../strategies/jwt.strategy";
import { FlightDraftModule } from "../flight-draft/flight-draft.module";
import { AircraftModule } from "src/core/modules/aircraft/aircraft.module";
import {
  Aircraft,
  AircraftSchema,
} from "src/core/schemas/aircraft/aircraft.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Flight.name, schema: FlightSchema },
      { name: Aircraft.name, schema: AircraftSchema },
    ]),
    UserModule,
    ConfigModule,
    FlightDraftModule,
    AircraftModule,
  ],
  providers: [FlightService, JwtStrategy],
  controllers: [FlightController],
})
export class FlightModule {}
