import {
  Controller,
  Get,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Request,
  Post,
  Param,
  UseInterceptors,
  Delete,
  Body,
  Put,
} from "@nestjs/common";
import { ApiBearerAuth, ApiParam, ApiTags } from "@nestjs/swagger";
import { SanitizeMongooseModelInterceptor } from "nestjs-mongoose-exclude";
import { UpdateFlightDto } from "src/core/modules/flight/models/dto/updateFlight.dto";
import { Flight } from "src/core/schemas/flight/flight.schema";

import { JwtAuthGuard } from "../../../guards/jwt-auth.guard";
import { ParseObjectIdPipe } from "../../../pipes/parseObjectId.pipe";
import { FlightService } from "../service/flight.service";

@Controller("flight")
@ApiTags("flight")
@UsePipes(new ValidationPipe({ transform: true }))
@UseInterceptors(
  new SanitizeMongooseModelInterceptor({
    excludeMongooseId: false,
    excludeMongooseV: true,
  })
)
export class FlightController {
  constructor(private readonly flightService: FlightService) {}

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get("all")
  async getAll(@Request() req) {
    const { _id } = req.user;
    return await this.flightService.findAll(_id);
  }

  @ApiBearerAuth()
  @ApiParam({ type: String, name: "flightId" })
  @UseGuards(JwtAuthGuard)
  @Get(":flightId")
  async getById(
    @Request() req,
    @Param("flightId", ParseObjectIdPipe) flightId: string
  ) {
    const { _id } = req.user;
    return await this.flightService.getById(_id, flightId);
  }

  @ApiBearerAuth()
  @ApiParam({ type: String, name: "draftId" })
  @UseGuards(JwtAuthGuard)
  @Put(":draftId")
  async updateFlight(
    @Param("draftId", ParseObjectIdPipe) draftId: string,
    @Request() req,
    @Body() dto: UpdateFlightDto
  ): Promise<Flight> {
    const { _id } = req.user;
    return await this.flightService.updateFlight(draftId, _id, dto);
  }

  @ApiBearerAuth()
  @ApiParam({ type: String, name: "flightId" })
  @UseGuards(JwtAuthGuard)
  @Delete(":flightId")
  async delete(
    @Request() req,
    @Param("flightId", ParseObjectIdPipe) flightId: string
  ) {
    const { _id } = req.user;
    return await this.flightService.deleteFlight(_id, flightId);
  }

  @ApiBearerAuth()
  @ApiParam({ type: String, name: "draftId" })
  @UseGuards(JwtAuthGuard)
  @Post("create/:draftId")
  async createFlight(
    @Request() req,
    @Param("draftId", ParseObjectIdPipe) draftId: string
  ) {
    const { _id } = req.user;

    return await this.flightService.createFlight(_id, draftId);
  }
}
