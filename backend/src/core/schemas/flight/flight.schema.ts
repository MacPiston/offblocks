import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import * as mongoose from "mongoose";

import { User } from "../user/user.schema";
import { Aircraft } from "../aircraft/aircraft.schema";
import { FlightDraft } from "../flight-draft/flightDraft.schema";

import { PathPoint, PathPointSchema } from "./subschemas/pathpoint";
import { PilotTime, PilotTimeSchema } from "./subschemas/pilotTime";
import { Landings, LandingsSchema } from "./subschemas/landings";
import {
  OperationalConditionTime,
  OperationalConditionTimeSchema,
} from "./subschemas/operationalConditionTime";
import {
  PilotFunctionTime,
  PilotFunctionTimeSchema,
} from "./subschemas/pilotFunctionTime";
import {
  SyntheticTrainingDeviceSession,
  SyntheticTrainingDeviceSessionSchema,
} from "./subschemas/synteticTrainingDeviceSession";

export type FlightDocument = Flight & Document;

@Schema()
export class Flight {
  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: "User" })
  owner: User;

  @Prop({
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: "FlightDraft",
  })
  createdFrom: FlightDraft;

  @Prop({ required: true, type: Date })
  createdOn: Date;

  @Prop({ required: true })
  departureDate: Date;

  @Prop({ type: PathPointSchema, default: {}, required: true })
  departurePoint: PathPoint;

  @Prop({ type: [PathPointSchema], default: [] })
  pathPoints?: PathPoint[];

  @Prop({ required: true })
  arrivalDate: Date;

  @Prop({ type: PathPointSchema, default: {}, required: true })
  arrivalPoint: PathPoint;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: "Aircraft",
    required: true,
  })
  aircraft: Aircraft;

  @Prop({ required: true })
  pic: string;

  @Prop({ type: LandingsSchema, default: {}, required: true })
  landings: Landings;

  @Prop({ type: PilotTimeSchema, default: {} })
  pilotTime: PilotTime;

  @Prop({ type: PilotFunctionTimeSchema, default: {} })
  pft: PilotFunctionTime;

  @Prop({ type: OperationalConditionTimeSchema, default: {} })
  oct: OperationalConditionTime;

  @Prop({ required: true })
  ifrApproaches: number;

  @Prop({ type: SyntheticTrainingDeviceSessionSchema, default: {} })
  stds: SyntheticTrainingDeviceSession;

  @Prop({ type: String })
  remarks?: string;
}

export const FlightSchema = SchemaFactory.createForClass(Flight);
