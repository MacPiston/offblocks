import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { IsInt, IsOptional } from "class-validator";

@Schema()
export class PilotTime {
  @Prop()
  @IsOptional()
  @IsInt()
  spse?: number;

  @Prop()
  @IsOptional()
  @IsInt()
  spme?: number;

  @Prop()
  @IsOptional()
  @IsInt()
  mp?: number;
}

export const PilotTimeSchema = SchemaFactory.createForClass(PilotTime);
