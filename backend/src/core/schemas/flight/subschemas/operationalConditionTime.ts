import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { IsInt, IsOptional, Min } from "class-validator";

@Schema()
export class OperationalConditionTime {
  @Prop()
  @IsOptional()
  @IsInt()
  @Min(0)
  night?: number;

  @Prop()
  @IsOptional()
  @IsInt()
  @Min(0)
  ifr?: number;
}

export const OperationalConditionTimeSchema = SchemaFactory.createForClass(
  OperationalConditionTime
);
